(function () {

    var gulp = require('gulp');
    var mocha = require('gulp-mocha');
    var istanbul = require('gulp-istanbul');

    //moved these to functions instead of gulp
    function test(cb) {
        gulp.src(['tests/**/*.js'])
            .pipe(istanbul({includeUntested: true})) // Covering files
            .pipe(istanbul.hookRequire()) // Force `require` to return covered files
            .on('finish', function () {
                gulp.src(['test/**/*.js'])
                    .pipe(mocha())
                    .pipe(istanbul.writeReports(
                        {
                            dir: './coverage',
                            reporters: ['html', 'lcov', 'json', 'text', 'text-summary', 'cobertura']
                        })) // Creating the reports after tests runned
                    .on('end', cb);
            });
    };

    gulp.task('test', function (cb) {
        test(cb);
    });

})();