const mongoose = require("mongoose");

const port = "27017"
const uri = "mongodb://mongo:27017/app";
const db = mongoose.connection;

mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).catch(err => console.log(err))

db.once('open', _ => {
    console.log("Database connected to: ", uri)
})

db.on("error", err => {
    console.log(err);
  });