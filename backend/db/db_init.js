require("./db");

const User = require("../src/models/User");
const Role = require("../src/models/Role");
const Course = require("../src/models/Course");
const Team = require("../src/models/Team");
const Account = require("../src/models/Account");
const Log = require("../src/models/Log");

const createUsers = async () => {
  const bcrypt = require("bcrypt");
  const superRole = await Role.findOne({name:"super"}).exec();  
  const rol = await Role.findOne({name:"admin"}).exec();
  const std = await Role.findOne({name:"user"}).exec();
  const salt = await bcrypt.genSalt(10);
  const passEncrypt = await bcrypt.hash("123456", salt);

  const superAdmin = new User({
    id:1,
    username: "super",
    password: passEncrypt,
    roles:[superRole._id]
  });

  const userAdmin = new User({
    id:2,
    username: "admin",
    password: passEncrypt,
    roles:[rol._id]
  });

  const userNormal1 = new User({
    id:3,
    username: "arturo",
    password: passEncrypt,
    roles:[std._id]
  });  

  const userNormal2 = new User({
    id:4,
    username: "jose",
    password: passEncrypt,
    roles:[std._id]
  });   

  const userNormal3 = new User({
    id:5,
    username: "jose ivan",
    password: passEncrypt,
    roles:[std._id]
  });     

  const userNormal4 = new User({
    id:6,
    username: "antonio",
    password: passEncrypt,
    roles:[std._id]
  });    
  
  const userNormal5 = new User({
    id:7,
    username: "Gonzalo",
    password: passEncrypt,
    roles:[std._id]
  });       
  
  const googleUser = new User({
    id:8,
    username: "Google",
    password: passEncrypt,
    roles:[std._id]
  });    

  await superAdmin.save();
  superRole.users.push(superAdmin._id);
  await superRole.save();  

  await userAdmin.save();
  rol.users.push(userAdmin._id);
  await rol.save();

  await userNormal1.save();
  std.users.push(userNormal1._id);

  await userNormal2.save();
  std.users.push(userNormal2._id);

  await userNormal3.save();
  std.users.push(userNormal3._id);  
  
  await userNormal4.save();
  std.users.push(userNormal4._id);  
  
  await userNormal5.save();
  std.users.push(userNormal5._id);      
  
  await googleUser.save();
  std.users.push(googleUser._id);
  await std.save();     
}

const createRoles = async () => {
  const superRole = new Role({
    id: 1,
    name: "super",
    permissions: []
  });

  await superRole.save();  

  const adminRole = new Role({
    id: 2,
    name: "admin",
    permissions: []
  });

  await adminRole.save();

  const modRole = new Role({
    id: 3,
    name: "mod",
    permissions: []
  });

  await modRole.save();

  const userRole = new Role({
    id: 4,
    name: "user",
    permissions: []
  });

  await userRole.save();
}

const createCourses = async () => {
  const adminUser = await User.findOne({id:1}).exec();
  const stdUser1 = await User.findOne({id:2}).exec();
  const stdUser2 = await User.findOne({id:3}).exec();

  const course1 = new Course({
    id:1,
    name: "React course",
    author: "Facebook",
    students:[stdUser1._id]
  });   
  
  const course2 = new Course({
    id:2,
    name: "Angular course",
    author: "Google",
    students:[stdUser2._id]
  });      

  const course3 = new Course({
    id:3,
    name: "Python course",
    author: "Python",
    students:[stdUser1._id, stdUser2._id]
  });   

  await course1.save();
  await course2.save();
  await course3.save();
  stdUser1.courses.push(course1._id);
  stdUser1.courses.push(course3._id);
  stdUser2.courses.push(course2._id);
  stdUser2.courses.push(course3._id);
  await stdUser1.save();
  await stdUser2.save();
}

const createTeams = async () => {
  const superUser = await User.findOne({id:1}).exec();
  const adminUser = await User.findOne({id:2}).exec();
  const stdUser1 = await User.findOne({id:3}).exec();
  const stdUser2 = await User.findOne({id:4}).exec();
  const stdUser3 = await User.findOne({id:5}).exec();
  const stdUser4 = await User.findOne({id:6}).exec();
  const stdUser5 = await User.findOne({id:7}).exec();

  const team1 = new Team({
    id:1,
    name: "Cobol team",
    users: [stdUser1._id, stdUser2._id]
  });   

  const team2 = new Team({
    id:2,
    name: "Backend team",
    users: [stdUser3._id, stdUser4._id]
  });   
  
  const team3 = new Team({
    id:3,
    name: "Frondend team",
    users: [stdUser5._id]
  });  
  

  await team1.save();
  await team2.save();
  await team3.save();

  stdUser1.team = team1._id;
  stdUser2.team = team1._id;
  stdUser3.team = team2._id;
  stdUser4.team = team2._id;
  stdUser5.team = team3._id;

  await stdUser1.save();
  await stdUser2.save();
  await stdUser3.save();
  await stdUser4.save();
  await stdUser5.save();  
}

const createAccounts = async () => {
  const team1 = await Team.findOne({id:1}).exec();
  const team2 = await Team.findOne({id:2}).exec();
  const team3 = await Team.findOne({id:3}).exec();
  const superUser = await User.findOne({id:1}).exec();
  const adminUser = await User.findOne({id:2}).exec();


  const ac1 = new Account({
    id: 1,
    account: "Bank of America",
    client: "James Doe",
    opresponsible: adminUser._id,
    teams: [team1._id, team2._id]
  });   

  const ac2 = new Account({
    id: 2,
    account: "US Bancorp",
    client: "John Williams",
    opresponsible: superUser._id,
    teams: [team3._id]
  });    

  await ac1.save();
  await ac2.save();
}

const createLogs = async () => {
  const stdUser3 = await User.findOne({id:3}).populate("team").exec();
  const stdUser2 = await User.findOne({id:4}).populate("team").exec();

  let startDate1 = new Date();
  startDate1.setDate(startDate1.getDate()-30);
  let startDate2 = new Date();
  startDate2.setDate(startDate2.getDate()-15);  
  let endDate = new Date();
  const log1 = new Log({
    id: 1,
    user: stdUser2._id,
    team: stdUser2.team._id,
    start_date: startDate1,
    end_date: endDate
  });   

  const log2 = new Log({
    id: 2,
    user: stdUser3._id,
    team: stdUser3.team._id,
    start_date: startDate2,
    end_date: endDate
  });     

  await log1.save();
  await log2.save();
}


const start =  async () => { 
  await createRoles()
  await createUsers()
  await createTeams()
  await createAccounts()
  await createLogs();
  const user = await User.findOne({username:"arturo"}).populate("roles").populate("team");
  console.log(user)
  const team = await Team.findOne({id:1}).populate("users");
  console.log(team)
  const ac = await Account.findOne({id:1}).populate("opresponsible").populate("teams");
  console.log(ac)  
}

start()