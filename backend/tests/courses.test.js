const request = require("supertest");

const app = require("../src/app");
const expect = require('chai').expect;

describe("Endpoint /courses/", () => {
  let adminUserToken = {};
  let userUserToken = {};
  before(async () => {
    let res1 = await request(app).post("/v2/users/login").send({ "username": "admin", "password": "123456" })
    adminUserToken = res1.body.token
    let res2 = await request(app).post("/v2/users/login").send({ "username": "arturo", "password": "123456" })
    userUserToken = res2.body.token
  })


  it("GET /courses/ | Get all courses with user role", (done) => {
    request(app)
      .get("/v2/courses")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + userUserToken)
      .expect("Content-Type", /json/)
      .expect(res => {
        if (!Array.isArray(res.body)) throw new Error("Incorrect response body: " + JSON.stringify(res.body))
      })
      .end(done);
    ;
  });

  it("GET /courses/:id | Get user with an id with user role", (done) => {
    request(app)
      .get("/v2/courses/1")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + userUserToken)
      .expect("Content-Type", /json/)
      .end(done);
    ;
  });  

  it("PUT /courses/:id | Update course", (done) => {
    request(app)
      .put("/v2/courses/3")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + adminUserToken)
      .send({id:3,name:"test"})
      .expect(204, done);
  });    

  it("POST /courses/ | Add a course", async () => {
    await request(app)
    .post("/v2/courses/")
    .set("Accept", "application/json")
    .set("Authorization", "Bearer " + adminUserToken)
    .send({id:990,name:"testDELETE"})
    .expect(200)
  
    await request(app)
      .delete("/v2/courses/990")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + adminUserToken)
    
   
     
  });   

  it("DELETE /courses/:id | Delete a course", async () => {
    await request(app)
    .post("/v2/courses/")
    .set("Accept", "application/json")
    .set("Authorization", "Bearer " + adminUserToken)
    .send({id:999,name:"testDELETE"})
 
  
    await request(app)
      .delete("/v2/courses/999")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + adminUserToken)
      .expect(204);
  });     


});
