const request = require("supertest");

const app = require("../src/app");
const expect = require('chai').expect;

describe("Endpoint /users/", () => {
  let adminUserToken = {};
  let userUserToken = {};
  before(async () => {
    let res1 = await request(app).post("/v2/users/login").send({ "username": "admin", "password": "123456" })
    adminUserToken = res1.body.token
    let res2 = await request(app).post("/v2/users/login").send({ "username": "arturo", "password": "123456" })
    userUserToken = res2.body.token
  })

  it("respond with json containing a single user", (done) => {

    request(app)
      .get("/v2/users")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + adminUserToken)
      .send()
      .expect("Content-Type", /json/)
      .expect(200, done);

    // .expect("Content-Type", /json/)
    ;
  });

  it("GET /users/ | Get all users with admin role", (done) => {
    request(app)
      .get("/v2/users")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + adminUserToken)
      .expect("Content-Type", /json/)
      .expect(res => {
        if (!Array.isArray(res.body)) throw new Error("Incorrect response body: " + JSON.stringify(res.body))
      })
      .end(done);
    ;
  });

  it("GET /users/:id | Get user with an id with admin role", (done) => {
    request(app)
      .get("/v2/users/1")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + adminUserToken)
      .expect("Content-Type", /json/)
      .end(done);
    ;
  });  

  it("PUT /users/:id | Update user with admin role", (done) => {
    request(app)
      .put("/v2/users/3")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + adminUserToken)
      .send({id:3,username:"test"})
      .expect(204, done);
  });    

  it("DELETE /users/:id | Delete user with admin role", async () => {
    await request(app)
    .post("/v2/users/")
    .set("Accept", "application/json")
    .set("Authorization", "Bearer " + adminUserToken)
    .send({id:999,username:"testDELETE",password:"123456"})
  
    await request(app)
      .delete("/v2/users/999")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + adminUserToken)
      .expect(204);
  });     

  it("POST /users/register | Register a user", async () => {
    await request(app)
    .post("/v2/users/register")
    .set("Accept", "application/json")
    .send({id:998,username:"testDelete",password:"123456"})
    .expect("Content-Type", /json/)
    .expect(200);
    
    await request(app)
      .delete("/v2/users/998")
      .set("Accept", "application/json")
      .set("Authorization", "Bearer " + adminUserToken)
    
  });    

  it("POST /users/login | Login a user", async () => {
    await request(app)
    .post("/v2/users/login")
    .set("Accept", "application/json")
    .send({username:"admin",password:"123456"})
    .expect("Content-Type", /json/)
    .expect(200);
    
  });   

});
