/**
 *@swagger
 * components:
 *   schemas:
 *     Team:
 *       type: object
 *       required:
 *         - id
 *         - name
 *         - author
 *       properties:
 *         id:
 *           type: integer
 *           description: The auto-generated id of the team
 *         name:
 *           type: string
 *           description: name of the team
 *         users:
 *           type: Array
 *           description: users assigned to the team
 * 
 * tags:
 *   - name: Teams
 *     description: API to manage app teams
 * 
 * paths:
 *  /v2/teams/:
 *    get:
 *      summary: Lists all the teams
 *      tags: [Teams]
 *      responses:
 *        "200":
 *          description: The list of teams
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Team'
 *              example:
 *                - id: 1
 *                  name: Cobol team
 *                  users: ['54534544323','6543632']
 *                - id: 2
 *                  name: Frondend team
 *                  users: ['43424343242','345432']
 *    post:                  
 *      summary: Creates a new team
 *      tags: [Teams]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Team'
 *            example:
 *              id: 2
 *              name: Frondend team
 *              students: ['4342432','345']
 *      responses:
 *        "201":
 *          description: Team created
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Team'
 *              example:
 *                id: 2
 *                name: Frondend team
 *                students: ['4342432','345']
 *  /v2/teams/{id}:
 *    get:
 *      summary: Gets a team by id
 *      tags: [Teams]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The team id
 *      responses:
 *        "200":
 *          description: Team found
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Team'
 *              example:
 *                id: 2
 *                name: Frondend team
 *                students: ['4342432','345']
 *        "404":
 *          description: Team not found
 *    put:
 *      summary: Updates a team
 *      tags: [Teams]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The team id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Team'
 *            example:
 *              id: 2
 *              name: Frondend team
 *              students: ['4342432','345']
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Team not found.
 *    delete:
 *      summary: Deletes a team by id
 *      tags: [Teams]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The team id
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Team not found.
 */
const express = require("express");
const { jwt, _, secretKey } = require("../utils/vars");

const router = express.Router();
const Team = require("../models/Team");
let currentUser = {};

router.get("/teams/", verifyAdmin, async (req, res) => {
    const teams = await Team.find().populate("users").exec();
    res.status(200).json(teams);
});

router.get("/teams/:id", verifyAdmin, async (req, res) => {
    const team = await Team.findOne({ id: req.params.id }).exec();

    team ? res.status(200).json(team) : res.contentType("text/plain").status(404).end("Team not found");
});


router.get("/teams/:name", verifyAdmin, async (req, res) => {
    const team = await Team.findOne({ name: req.params.name }).exec();

    team ? res.status(200).json(team) : res.contentType("text/plain").status(404).end("Team not found");
});

router.post("/teams", verifyAdmin, async (req, res) => {
    if (Array.isArray(req.body)) {
        let records = req.body;
        let result = await Promise.all(records.map(async team => {
            let tm = {};
            let tmRes = {};
            tm.id = team.id ? team.id : null;
            tm.name = team.name;
            team.users ? tm.users = await Promise.all(team.users.map(async user => (await User.findOne({ username: user }).exec())._id)) : null;
            tmRes.id = team.id ? team.id : null;
            tmRes.name = team.name;
            team.users ? tmRes.users = await Promise.all(team.users.map(async user => (await User.findOne({ username: user }).exec())._id)) : null;
            const teamORM = new Team(tm);
            await teamORM.save();
            return tmRes;
        }));
        res.status(200).json(result)

    }
    else {
        let team = req.body;
        let tm = {};
        let tmRes = {};
        tm.id = team.id ? team.id : null;
        tm.name = team.name;
        team.users ? tm.users = await Promise.all(team.users.map(async user => (await User.findOne({ username: user }).exec())._id)) : null;
        tmRes.id = team.id ? team.id : null;
        tmRes.name = team.name;
        team.users ? tmRes.users = await Promise.all(team.users.map(async user => (await User.findOne({ username: user }).exec())._id)) : null;
        const teamORM = new Team(tm);
        await teamORM.save();
        res.status(200).json(tmRes)
    }

});

router.put("/teams/:id", verifyAdmin, async (req, res) => {
    const team = await Team.findOne({ id: req.params.id }).exec();
    if (team) {
        req.body.users ? team.users = await Promise.all(tm.users.map(async user => (await User.findOne({ id: user }).exec())._id)) : null;
        req.body.name ? team.name = req.body.name : null;
        await team.save()
        res.status(204).send(team);
        res.sendStatus(404);
    } else {
        res.sendStatus(404);
    }
});

router.delete("/teams/:id", verifyAdmin, async (req, res) => {
    const team = await Team.findOne({ id: req.params.id }).exec();

    if (team) {
        team.deleteOne();
        res.sendStatus(204);
    } else {
        return res.sendStatus(404);
    }
});


function verifyAdmin(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
            if (error) {
                console.log(error)
                currentUser = {};
                res.sendStatus(403);
            } else {
                if (_.find(authData.user.roles, function (role) { return role.name == "admin" || role.name == "super"; }) ? true : false)
                    next();
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

function verifyUser(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
            if (error) {
                currentUser = {};
                console.log(error)
                res.sendStatus(403);
            } else {
                currentUser = authData.user;

                if (_.find(authData.user.roles, function (role) { return role.name == "admin" || role.name == "user" || role.name == "mod"; }) ? true : false)
                    next();
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

module.exports = router;
