/**
 *@swagger
 * components:
 *   schemas:
 *     Log:
 *       type: object
 *       required:
 *         - id
 *         - name
 *         - author
 *       properties:
 *         id:
 *           type: integer
 *           description: The auto-generated id of the course.
 *         name:
 *           type: string
 *           description: name of the course.
 *         author:
 *           type: string
 *           description: author of the course
 *         students:
 *           type: Array
 *           description: students registered to the course
 * 
 * tags:
 *   - name: Logs
 *     description: API to manage app logs
 * 
 * paths:
 *  /v2/logs/:
 *    get:
 *      summary: Lists all the logs
 *      tags: [Logs]
 *      responses:
 *        "200":
 *          description: The list of logs.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Log'
 *              example:
 *                - id: 1
 *                  name: React course
 *                  author: Facebook
 *                  students: ['54534543','65632']
 *                - id: 2
 *                  name: Angular course
 *                  author: Google
 *                  students: ['4342432','345']
 *    post:                  
 *      summary: Creates a new course
 *      tags: [Logs]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Log'
 *            example:
 *              id: 2
 *              name: course
 *              author: Google
 *              students: ['4342432','345']
 *      responses:
 *        "201":
 *          description: Log created
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Log'
 *              example:
 *                id: 2
 *                name: course
 *                author: Google
 *                students: ['4342432','345']
 *  /v2/logs/{id}:
 *    get:
 *      summary: Gets a course by id
 *      tags: [Logs]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The course id
 *      responses:
 *        "200":
 *          description: Log found
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Log'
 *              example:
 *                id: 2
 *                name: course
 *                author: Google
 *                students: ['4342432','345']
 *        "404":
 *          description: Log not found
 *    put:
 *      summary: Updates a course
 *      tags: [Logs]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The course id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Log'
 *            example:
 *              id: 2
 *              name: course
 *              author: Google
 *              students: ['4342432','345']
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Log not found.
 *    delete:
 *      summary: Deletes a course by id
 *      tags: [Logs]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The course id
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Log not found.
 */
const express = require("express");
const { jwt, _, secretKey } = require("../utils/vars");


const router = express.Router();
const Log = require("../models/Log");
const Team = require("../models/Team");
const User = require("../models/User");
let currentUser = {};

router.get("/logs/", verifyAdmin, async (req, res) => {
    const logs = await Log.find().populate("user").populate("team").exec();
    res.status(200).json(logs);
});

router.get("/logs/:id", verifyAdmin, async (req, res) => {
    const log = await Log.findOne({ id: req.params.id }).exec();

    log ? res.status(200).json(log) : res.contentType("text/plain").status(404).end("Log not found");
});



router.get("/logs/:user", verifyAdmin, async (req, res) => {
    const user = await User.findOne({ id: req.params.user }).exec();
    const logs = await Log.find({ user: user._id }).exec();

    logs ? res.status(200).json(logs) : res.contentType("text/plain").status(404).end("Logs not found");
});

router.post("/logs/", verifyAdmin, async (req, res) => {
    if (Array.isArray(req.body)) {
        let lgs = req.body;
        let logsUpdated = [];
        let resp = await Promise.all(lgs.map(async log => {
            let logExists = await Log.findOne({ id: log.id }).exec();
            let lg = {};
            let logRes = {};
            if (!!logExists) {
                lg = logExists;
            }


            lg.id = log.id ? log.id : null;
            lg.user = log.user ? (await User.findOne({ username: log.user }).exec())._id : null;

            lg.team = log.team ? (await Team.findOne({ name: log.team }).exec())._id : null;
            lg.start_date = log.start_date ? new Date(log.start_date) : null;
            lg.end_date = log.end_date ? new Date(log.end_date) : null;

            logRes.id = log.id ? log.id : null;
            logRes.user = log.user ? (await User.findOne({ _id: lg.user }).exec()).username : null;

            logRes.team = log.team ? (await Team.findOne({ _id: lg.team }).exec()).name : null;
            logRes.start_date = log.start_date ? new Date(log.start_date) : null;
            logRes.end_date = log.end_date ? new Date(log.end_date) : null;
            if (!!logExists) {
                await lg.save();
                logsUpdated.push(lg.id);
            }
            else {
                
                const logORM = new Log(lg);
                await logORM.save();
                console.log(logORM)
                logsUpdated.push(logORM.id);
            }
            return logRes;
        }));
        await Log.deleteMany({ id: { $nin: logsUpdated } }).exec();
        res.status(200).json(resp);
    }
    else {
        let lgs = req.body;
        let logsUpdated = [];

        let logExists = await Log.findOne({ id: log.id }).exec();
        let lg = {};
        let logRes = {};
        if (!!logExists) {
            lg = logExists;
        }

        lg.id = log.id ? log.id : null;
        lg.user = log.user ? (await User.findOne({ username: log.user }).exec())._id : null;

        lg.team = log.team ? (await Team.findOne({ name: log.team }).exec())._id : null;
        lg.start_date = log.start_date ? new Date(log.createdAt) : null;
        lg.end_date = log.end_date ? new Date(log.createdAt) : null;

        logRes.id = log.id ? log.id : null;
        logRes.user = log.user ? (await User.findOne({ _id: lg.user }).exec()).username : null;

        logRes.team = log.team ? (await Team.findOne({ _id: lg.team }).exec()).name : null;
        logRes.start_date = log.start_date ? new Date(log.createdAt) : null;
        logRes.end_date = log.end_date ? new Date(log.createdAt) : null;

        if (!!logExists) {
            await lg.save();
            logsUpdated.push(lg.id);
        }
        else {

            const logORM = new Log(lg);
            await logORM.save();
            logsUpdated.push(logORM.id);
        }


        await Log.deleteMany({ id: { $nin: logsUpdated } }).exec();
        res.status(200).json(logRes);
    }
});

router.put("/logs/:id", verifyAdmin, async (req, res) => {
    const log = await Log.findOne({ id: req.params.id }).exec();
    if (log) {
        req.body.user ? log.user = (await User.findOne({ id: req.body.user }).exec())._id : null;
        req.body.team ? log.team = (await Team.findOne({ id: req.body.team }).exec())._id : null;
        req.body.start_date ? log.start_date = req.body.start_date : null;
        req.body.end_date ? log.end_date = req.body.end_date : null;
        await log.save();
        res.status(200).json(log);
        res.sendStatus(404);
    } else {
        res.sendStatus(404);
    }
});

router.delete("/logs/:id", verifyAdmin, async (req, res) => {
    const log = await Log.findOne({ id: req.params.id }).exec();

    if (log) {
        log.deleteOne()
        res.sendStatus(204);
    } else {
        return res.sendStatus(404);
    }
});


function verifyAdmin(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
            if (error) {
                console.log(error)
                currentUser = {};
                res.sendStatus(403);
            } else {

                if (_.find(authData.user.roles, function (role) { return role.name == "admin" || role.name == "super"; }) ? true : false)
                    next();
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

function verifyUser(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
            if (error) {
                currentUser = {};
                console.log(error)
                res.sendStatus(403);
            } else {
                currentUser = authData.user;

                if (_.find(authData.user.roles, function (role) { return role.name == "admin" || role.name == "user" || role.name == "mod"; }) ? true : false)
                    next();
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

module.exports = router;
