/**
 *@swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       required:
 *         - id
 *         - username
 *         - password
 *         - roles
 *         - courses
 *         - createdAt
 *       properties:
 *         id:
 *           type: integer
 *           description: The auto-generated id of the user.
 *         username:
 *           type: string
 *           description: username.
 *         password:
 *           type: string
 *           description: password of the user
 *         roles:
 *           type: Array
 *           description: roles assigned to the user
 *         courses:
 *           type: Array
 *           description: courses registered by the user
 *         createdAt:
 *           type: string
 *           format: date
 *           description: The date of the record creation.
 * 
 * tags:
 *   - name: Users
 *     description: API to manage app users
 * 
 * paths:
 *  /v2/users/:
 *    get:
 *      summary: Lists all the users
 *      tags: [Users]
 *      responses:
 *        "200":
 *          description: The list of users.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/User'
 *              example:
 *                - id: 2
 *                  username: user
 *                  password: 12356
 *                  roles: ['32154347832473','4342432']
 *                  courses: ['55469879676','9879687976']
 *                  createdAt: 2010-10-10
  *                - id: 2
 *                  username: admin
 *                  password: 12356
 *                  roles: ['4342432']
 *                  courses: ['9879687976']
 *                  createdAt: 2021-10-10
 *    post:
 *      summary: Creates a new user
 *      tags: [Users]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *            example:
 *              id: 2
 *              username: user
 *              password: 12356
 *              roles: ['32154347832473','4342432']
 *              courses: ['55469879676','9879687976']
 *              createdAt: 2010-10-10
 *      responses:
 *        "201":
 *          description: User created
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/User'
 *              example:
 *                id: 2
 *                username: user
 *                password: 12356
 *                roles: ['32154347832473','4342432']
 *                courses: ['55469879676','9879687976']
 *                createdAt: 2010-10-10
 *  /v2/users/{id}:
 *    get:
 *      summary: Gets a user by id
 *      tags: [Users]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The user id
 *      responses:
 *        "200":
 *          description: The user.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/User'
 *              example:
 *                id: 2
 *                username: user
 *                password: 12356
 *                roles: ['32154347832473','4342432']
 *                courses: ['55469879676','9879687976']
 *                createdAt: 2010-10-10
 *        "404":
 *          description: User not found
 *    put:
 *      summary: Updates a user
 *      tags: [Users]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The user id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *            example:
 *              id: 2
 *              username: user
 *              password: 12356
 *              roles: ['32154347832473','4342432']
 *              courses: ['55469879676','9879687976']
 *              createdAt: 2010-10-10
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: User not found.
 *    delete:
 *      summary: Deletes a user by id
 *      tags: [Users]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The user id
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: User not found.
 */
const express = require("express");



const { jwt, bcrypt, _, secretKey, tokenExpires } = require("../utils/vars");

const router = express.Router();
const User = require("../models/User");
const Role = require("../models/Role");
const Team = require("../models/Team");
let currentUser = {};

router.get("/users/", verifyAdmin, async (req, res) => {
    const users = await User.find().populate("roles").populate("team").exec();
    let usersRes = users.map(user => {
  
        return { id: user.id, username: user.username, password: user.password, roles: !!user.roles && Array.isArray(user.roles) ? user.roles.map(role => role.name).join(",") : "", team: !!user.team ? user.team.name : "", createdAt: user.createdAt };
    });
    res.status(200).json(usersRes);
});

router.get("/users/:id", verifyAdmin, async (req, res) => {
    const user = await User.findOne({ id: req.params.id }).exec();
    if (user) {
        res.status(200).send(user);
    } else {
        res.status(404).send("User not found");
    }
});

router.get("/users/:username", verifyAdmin, async (req, res) => {
    const user = await User.findOne({ username: req.params.username }).exec();
    if (user) {
        res.status(200).send(user);
    } else {
        res.status(404).send("User not found");
    }
});

router.post("/users/register", async (req, res) => {
    const user = new User(req.body);
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
    res.status(200).json(user);
});


router.post("/users/", verifyAdmin, async (req, res) => {
    if (Array.isArray(req.body)) {
        let usrs = req.body;
        let usersUpdated = [];
        const salt = await bcrypt.genSalt(10);
        let resp = await Promise.all(usrs.map(async user => {
            let userExists = await User.findOne({ id: user.id }).exec();
            
    
            let us = {};
            if (!!userExists) {
                us = userExists;
                us.password != user.password ? await bcrypt.hash(user.password, salt) : null;
            }
            else
                us.password =  await bcrypt.hash(user.password, salt);

            let usRes = {};
            us.id = user.id ? user.id : null;
            us.username = user.username ? user.username : null;
            
            us.roles = user.roles ? await Promise.all(user.roles.split(",").map(async role => (await Role.findOne({ name: role }).exec())._id)) : null;
            us.team = user.team ? (await Team.findOne({ name: user.team }).exec())._id : null;
            us.createdAt = user.createdAt ? new Date(user.createdAt)  : null;

            usRes.id = user.id ? user.id : null;
            usRes.username = user.username ? user.username : null;
            usRes.password = user.password ? await bcrypt.hash(user.password, salt) : null;
            usRes.roles = user.roles ? await Promise.all(us.roles.map(async role => (await Role.findOne({ _id: role }).exec()).name)) : null;
            usRes.team = user.team ? (await Team.findOne({ _id: us.team }).exec()).name : null;
            usRes.createdAt = user.createdAt ? new Date(user.createdAt)  : null;

            if (!!userExists) {
                await us.save();
                usersUpdated.push(us.id);
            }
            else {
                const userORM = new User(us);
                await userORM.save();
                usersUpdated.push(us.id);
            }
            return usRes;
        }));
        await User.deleteMany({ id:{ $nin: usersUpdated } }).exec();
        res.status(200).json(resp);
    }
    else {
        let userExists = User.findOne({ id: user.id }).exec();
        let us = {};
        if (!!userExists) {
            us = userExists;
            us.password = us.password != user.password ? await bcrypt.hash(user.password, salt) : us.password;
        }
        else
            us.password =  await bcrypt.hash(user.password, salt) ;
        let usRes = {};
        us.id = req.body.id ? req.body.id : null;
        us.username = req.body.username ? req.body.username : null;
        
        us.password = req.body.password ? await bcrypt.hash(req.body.password, salt) : null;
        us.roles = req.body.roles ? await Promise.all(req.body.roles.split(",").map(async role => (await Role.findOne({ name: role }).exec())._id)) : null;
        us.team = rq.body.team ? (await Team.findOne({ name: req.body.team }).exec())._id : null;
        us.createdAt = user.createdAt ? new Date(user.createdAt)  : null;
        
        usRes.id = user.id ? user.id : null;
        usRes.username = user.username ? user.username : null;
        usRes.password = user.password ? await bcrypt.hash(user.password, salt) : null;
        usRes.roles = user.roles ? await Promise.all(us.roles.map(async role => (await Role.findOne({ _id: role }).exec()).name)) : null;
        usRes.team = user.team ? (await Team.findOne({ _id: us.team }).exec()).name : null;
        usRes.createdAt = user.createdAt ? new Date(user.createdAt)  : null;

        if (!!userExists)
            us.save();
        else {
            const userORM = new User(us);
            await userORM.save();
        }
        res.status(200).json(usRes);
    }
});

router.put("/users/:id", verifyAdmin, async (req, res) => {
    const user = await User.findOne({ id: req.params.id }).exec();
    if (user) {
        let us = {};
        const salt = await bcrypt.genSalt(10);

        user.username = req.body.username ? req.body.username : null;
        user.password = req.body.password ? await bcrypt.hash(req.body.password, salt) : null;
        user.roles = req.body.roles ? await Promise.all(req.body.roles.map(async role => (await Role.findOne({ id: role }).exec())._id)) : null;
        user.team = req.body.team ? (await Team.findOne({ id: req.body.team }).exec())._id : null;
 

        await user.save();
        res.status(200).json(user);
        res.sendStatus(204);
    } else {
        res.sendStatus(404);
    }
});

router.post("/users/login", async (req, res) => {

    //const userC = require("../app.currentUser")
    const body = req.body;
    const user = await User.findOne({ username: body.username }).populate("roles").exec();
    if (user) {
        // check user password with hashed password stored in the database
        const validPassword = await bcrypt.compare(body.password, user.password);
        if (validPassword) {
            jwt.sign({ user }, secretKey, { expiresIn: tokenExpires }, (err, token) => {
                currentUser = user;
                res.status(200).json({
                    id: user.id,
                    username: user.username,
                    roles: user.roles,
                    token,
                });
            });

        } else {
            res.status(400).json({ error: "Invalid Password" });
        }
    } else {
        res.status(401).json({ error: "User does not exist" });
    }
});


router.delete("/users/:id", verifyAdmin, async (req, res) => {
    const user = await User.findOne({ id: req.params.id }).exec();

    if (user) {
        user.deleteOne()
        res.status(204).json(user);
    } else {
        return res.sendStatus(404);
    }
}
);

function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, (error, authData) => {
            if (error) {
         
                res.sendStatus(403);
            } else {
                next();
            }
        });

    } else {
        res.sendStatus(403);
    }
}

function verifyAdmin(req, res, next) {
 
    const bearerHeader = req.headers['authorization'];


    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
          
            if (error) {
        
                currentUser = {};
                res.sendStatus(403);
            } else {
          
                if (_.find(authData.user.roles, function (role) { return role.name == "admin" || role.name == "super"; }) ? true : false) {
                  
                    next();
                }
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

module.exports = router;
