/**
 *@swagger
 * components:
 *   schemas:
 *     Course:
 *       type: object
 *       required:
 *         - id
 *         - name
 *         - author
 *       properties:
 *         id:
 *           type: integer
 *           description: The auto-generated id of the course.
 *         name:
 *           type: string
 *           description: name of the course.
 *         author:
 *           type: string
 *           description: author of the course
 *         students:
 *           type: Array
 *           description: students registered to the course
 * 
 * tags:
 *   - name: Courses
 *     description: API to manage app courses
 * 
 * paths:
 *  /v2/courses/:
 *    get:
 *      summary: Lists all the courses
 *      tags: [Courses]
 *      responses:
 *        "200":
 *          description: The list of courses.
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Course'
 *              example:
 *                - id: 1
 *                  name: React course
 *                  author: Facebook
 *                  students: ['54534543','65632']
 *                - id: 2
 *                  name: Angular course
 *                  author: Google
 *                  students: ['4342432','345']
 *    post:                  
 *      summary: Creates a new course
 *      tags: [Courses]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Course'
 *            example:
 *              id: 2
 *              name: course
 *              author: Google
 *              students: ['4342432','345']
 *      responses:
 *        "201":
 *          description: Course created
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Course'
 *              example:
 *                id: 2
 *                name: course
 *                author: Google
 *                students: ['4342432','345']
 *  /v2/courses/{id}:
 *    get:
 *      summary: Gets a course by id
 *      tags: [Courses]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The course id
 *      responses:
 *        "200":
 *          description: Course found
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Course'
 *              example:
 *                id: 2
 *                name: course
 *                author: Google
 *                students: ['4342432','345']
 *        "404":
 *          description: Course not found
 *    put:
 *      summary: Updates a course
 *      tags: [Courses]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The course id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Course'
 *            example:
 *              id: 2
 *              name: course
 *              author: Google
 *              students: ['4342432','345']
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Course not found.
 *    delete:
 *      summary: Deletes a course by id
 *      tags: [Courses]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The course id
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Course not found.
 */
const express = require("express");
const {jwt,_,secretKey} = require("../utils/vars");

const router = express.Router();
const Course = require("../models/Course");
let currentUser = {};




router.get("/courses/", verifyUser, async (req, res) => {
    const courses = await Course.find().exec();
    res.status(200).json(courses);
});

router.get("/courses/:id", verifyUser, async (req, res) => {
    const course = await Course.findOne({ id: req.params.id }).exec();

    course ? res.status(200).json(course) : res.contentType("text/plain").status(404).end("Course not found");
});

router.post("/courses/", verifyUser, async (req, res) => {
    const course = new Course({id:req.body.id, name: req.body.name, author: currentUser.username });
    await course.save();
    res.status(200).json(course);
    //res.contentType("text/plain").status(400).end("Course not created");
});

router.put("/courses/:id", verifyUser, async (req, res) => {
    const course = await Course.findOne({ id: req.params.id }).exec();
    if (course) {
        if(course.author == currentUser.username || _.find(currentUser.roles, function (role) { return role.name == "admin"; }))
        {
            course.name = req.body.name;
            await course.save()
            res.status(204).send(course);
        }
        res.sendStatus(404);
    } else {
        res.sendStatus(404);
    }
});

router.delete("/courses/:id", verifyUser, async (req, res) => {
    const course = await Course.findOne({ id: req.params.id }).exec();

    if (course) {
        if (course.author == currentUser.username || _.find(currentUser.roles, function (role) { return role.name == "admin"; })) {
            course.deleteOne()
            res.sendStatus(204);
        }
        else
            return res.sendStatus(404);
    } else {
        return res.sendStatus(404);
    }
});


function verifyAdmin(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
            if (error) {
                console.log(error)
                currentUser = {};
                res.sendStatus(403);
            } else {

                if (_.find(authData.user.roles, function (role) { return role.name == "admin"; }) ? true : false)
                    next();
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

function verifyUser(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
            if (error) {
                currentUser = {};
                console.log(error)
                res.sendStatus(403);
            } else {
                currentUser = authData.user;

                if (_.find(authData.user.roles, function (role) { return role.name == "admin" || role.name == "user" || role.name == "mod"; }) ? true : false)
                    next();
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

module.exports = router;
