/**
 *@swagger
 * components:
 *   schemas:
 *     Account:
 *       type: object
 *       required:
 *         - id
 *         - name
 *         - author
 *       properties:
 *         id:
 *           type: integer
 *           description: The auto-generated id of the account
 *         account:
 *           type: string
 *           description: Name of the account
 *         client:
 *           type: string
 *           description: Client in the account
 *         opresponsible:
 *           type: User
 *           description: User responsible to the operations of the account
 *         teams:
 *           type: Array
 *           description: teams assigned to the account
 * 
 * tags:
 *   - name: Accounts
 *     description: API to manage app accounts
 * 
 * paths:
 *  /v2/accounts/:
 *    get:
 *      summary: Lists all the accounts
 *      tags: [Accounts]
 *      responses:
 *        "200":
 *          description: The list of accounts
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Account'
 *              example:
 *                - id: 1
 *                  account: Bank of America
 *                  client: Frank Doe
 *                  opresponsible: '43424343242'
 *                  teams: ['43562665','876876909']  
 *                - id: 2
 *                  account: Truist Bank
 *                  client: Adam Brown
 *                  opresponsible: '5646576'
 *                  teams: ['78759659','9988']  
 *    post:                  
 *      summary: Creates a new account
 *      tags: [Accounts]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Account'
 *            example:
 *              id: 2
 *              account: Truist Bank
 *              client: Adam Brown
 *              opresponsible: '5646576'
 *              teams: ['78759659','9988']  
 *      responses:
 *        "201":
 *          description: Account created
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Account'
 *              example:
 *                id: 2
 *                account: Truist Bank
 *                client: Adam Brown
 *                opresponsible: '5646576'
 *                teams: ['78759659','9988']  
 *  /v2/accounts/{id}:
 *    get:
 *      summary: Gets a account by id
 *      tags: [Accounts]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The account id
 *      responses:
 *        "200":
 *          description: Account found
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Account'
 *              example:
 *                id: 2
 *                account: Truist Bank
 *                client: Adam Brown
 *                opresponsible: '5646576'
 *                teams: ['78759659','9988']  
 *        "404":
 *          description: Account not found
 *    put:
 *      summary: Updates a account
 *      tags: [Accounts]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The account id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Account'
 *            example:
 *              id: 2
 *              account: Truist Bank
 *              client: Adam Brown
 *              opresponsible: '5646576'
 *              teams: ['78759659','9988']  
 *      responses:
 *        "204":
 *          description: Update was successful.
 *        "404":
 *          description: Account not found.
 *    delete:
 *      summary: Deletes a account by id
 *      tags: [Accounts]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: integer
 *          required: true
 *          description: The account id
 *      responses:
 *        "204":
 *          description: Delete was successful.
 *        "404":
 *          description: Account not found.
 */
const express = require("express");
const { jwt, _, secretKey } = require("../utils/vars");

const router = express.Router();
const Account = require("../models/Account");
const Team = require("../models/Team");
const User = require("../models/User");
let currentUser = {};




router.get("/accounts/", verifyAdmin, async (req, res) => {
    const accounts = await Account.find().populate("opresponsible").populate("teams").exec();
    res.status(200).json(accounts);
});

router.get("/accounts/:id", verifyAdmin, async (req, res) => {
    const account = await Account.findOne({ id: req.params.id }).exec();

    account ? res.status(200).json(account) : res.contentType("text/plain").status(404).end("Account not found");
});

router.get("/accounts/:name", verifyAdmin, async (req, res) => {
    const account = await Account.findOne({ name: req.params.name }).exec();

    account ? res.status(200).json(account) : res.contentType("text/plain").status(404).end("Account not found");
});

router.post("/accounts/", verifyAdmin, async (req, res) => {
    console.log("aaasdsd111")
    if (Array.isArray(req.body)) {


        let records = req.body;
        let actUpdated = [];
        let resp = await Promise.all(records.map(async account => {
            let accountExists = await Account.findOne({ id: account.id }).exec();
            let act = {};
            let actRes = {};
            if (!!accountExists) {
                act = accountExists;
            }

            account.id ? act.id = account.id : null;
            account.client ? act.client = account.client : null;
            account.account ? act.account = account.account : null;
            account.opresponsible ? act.opresponsible = (await User.findOne({ username: account.opresponsible }).exec())._id : null;
            account.teams ? act.teams = await Promise.all(account.teams.split(",").map(async team => (await Team.findOne({ name: team }).exec())._id)) : null;

            account.id ? actRes.id = account.id : null;
            account.client ? actRes.client = account.client : null;
            account.account ? actRes.account = account.account : null;
            account.opresponsible ? actRes.opresponsible = (await User.findOne({ _id: act.opresponsible }).exec()).name : null;
            account.teams ? actRes.teams = await Promise.all(act.teams.map(async team => (await Team.findOne({ _id: team }).exec()).name)) : null;



            if (!!accountExists) {
                await act.save();
                actUpdated.push(account.id);
            }
            else {
                const accountORM = new User(us);
                await accountORM.save();
                actUpdated.push(account.id);
            }
        }));
        await Account.deleteMany({ id:{ $nin: actUpdated } }).exec();
        res.status(200).json(resp);
        //res.contentType("text/plain").status(400).end("Account not created");
    }
    else {


        let account = req.body;
        let accountExists = await Account.findOne({ id: account.id }).exec();
        let act = {};
        let actRes = {};
        if (!!accountExists) {
            act = accountExists;
        }

        account.id ? act.id = account.id : null;
        account.client ? act.client = account.client : null;
        account.account ? act.account = account.account : null;
        account.opresponsible ? act.opresponsible = (await User.findOne({ username: account.opresponsible }).exec())._id : null;
        account.teams ? act.teams = await Promise.all(account.teams.split(",").map(async team => (await Team.findOne({ name: team }).exec())._id)) : null;

        account.id ? actRes.id = account.id : null;
        account.client ? actRes.client = account.client : null;
        account.account ? actRes.account = account.account : null;
        account.opresponsible ? actRes.opresponsible = (await User.findOne({ _id: act.opresponsible }).exec()).name : null;
        account.teams ? actRes.teams = await Promise.all(act.teams.map(async team => (await Team.findOne({ _id: team }).exec()).name)) : null;

        if (!!accountExists) {

            await act.save();
        }
        else {
            const accountORM = new Account(act);
            await accountORM.save();
        }
        res.status(200).json(actRes);
    }

});

router.put("/accounts/:id", verifyAdmin, async (req, res) => {
    const account = await Account.findOne({ id: req.params.id }).exec();
    if (account) {
        req.body.name ? account.name = req.body.name : null;
        req.body.opresponsible ? account.opresponsible = (await User.findOne({ id: req.body.opresponsible }).exec())._id : null;
        req.body.client ? account.client = req.body.client : null;
        req.body.teams ? account.teams = await Promise.all(req.body.map(async team => (await Team.findOne({ id: team }).exec())._id)) : null;
        await account.save()
        res.status(204).send(account);
        res.sendStatus(404);
    } else {
        res.sendStatus(404);
    }
});

router.delete("/accounts/:id", verifyAdmin, async (req, res) => {
    const account = await Account.findOne({ id: req.params.id }).exec();
    if (account) {
        account.deleteOne();
        res.sendStatus(204);
    } else {
        return res.sendStatus(404);
    }
});


function verifyAdmin(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
            if (error) {
                console.log(error)
                currentUser = {};
                res.sendStatus(403);
            } else {

                if (_.find(authData.user.roles, function (role) { return role.name == "admin" || role.name == "super"; }) ? true : false)
                    next();
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

function verifyUser(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
            if (error) {
                currentUser = {};
                console.log(error)
                res.sendStatus(403);
            } else {
                currentUser = authData.user;

                if (_.find(authData.user.roles, function (role) { return role.name == "admin" || role.name == "user" || role.name == "mod"; }) ? true : false)
                    next();
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

module.exports = router;
