var express = require("express");
const swaggerUI = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");


require("../db/db");
require("../db/db_init");
const {swOptions} = require("./utils/vars");
const app = express();
const specs = swaggerJsDoc(swOptions);

app.use(express.json()); 
app.use(express.urlencoded({ extended: false }))
app.use("/docs",swaggerUI.serve,swaggerUI.setup(specs));

const errorHandler = (error, req, res, next) => {
  console.log(error.message);
  res.status(500).send(`Something wrong ${error.message}`);
};



//app.use(errorHandler);

//v1
app.use("/v1", require("./routes/users"));

//v2
app.use("/v2", require("./routes/users"));
app.use("/v2", require("./routes/courses"));
app.use("/v2", require("./routes/accounts"));
app.use("/v2", require("./routes/log"));


const PORT = process.env.PORT || 3001;
app.listen(PORT);

console.debug("Server listening on port: " + PORT);

module.exports = app;
