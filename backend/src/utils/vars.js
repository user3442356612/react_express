
const secretKey = "keyForExpressAPI";
const tokenExpires = "3000s";
var _ = require('lodash');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const swOptions = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "Express API with Swagger",
        version: "2.1.0",
        description:
          "Express CRUD API application made with Express/MongoDB/Swagger",
        license: {
          name: "MIT",
          url: "https://spdx.org/licenses/MIT.html",
        },
        contact: {
          name: "Arturo Santana Zamudio",
          url: "https://logrocket.com",
          email: "arturo.santana20xx@gmail.com",
        },
      },
      servers: [
        {
          url: "http://localhost:3000",
        },
      ],
    },
    apis: ["./src/routes/users.js","./src/routes/courses.js"],
  };

module.exports = {swOptions,bcrypt,jwt,_,secretKey,tokenExpires};