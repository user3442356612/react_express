
var _ = require('lodash');
const jwt = require("jsonwebtoken");
const secretKey = require("../utils/secretKey");

function verifyUser(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        jwt.verify(bearerToken, secretKey, async (error, authData) => {
            if (error) {
                currentUser = {};
                console.log(error)
                res.sendStatus(403);
            } else {
                currentUser = authData.user;
               
                if (_.find(authData.user.roles, function(role) { return role.name == "admin" || role.name == "user" || role.name == "mod"; })?true:false)
                    next();
                else
                    res.sendStatus(403);
            }
        });

    } else {
        res.sendStatus(403);
    }
}

module.exports = verifyUser;