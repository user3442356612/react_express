const { Schema, model } = require('mongoose');

const roleSchema = new Schema({
    id: {
        type: Number,
        unique: true,
        required: true
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    permissions: {
        type: Array,
        required: true,
    },
    users:[{
        type: Schema.Types.ObjectId,
        ref: "User"        
    }]
});

module.exports = new model('Role', roleSchema);