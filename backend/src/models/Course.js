const { Schema, model } = require('mongoose');

const courseSchema = new Schema({
    id: {
        type: Number,
        unique: true,
        required: true
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    author: {
        type: String,
        required: true,
    },
    students:[{
        type: Schema.Types.ObjectId,
        ref: "User",     
    }]    
});

module.exports = new model('Course', courseSchema);