const { Schema, model } = require('mongoose');

const logSchema = new Schema({
    id: {
        type: Number,
        unique: true,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",  
        required: true,           
    },   
    team: {
        type: Schema.Types.ObjectId,
        ref: "Team",  
        required: true,           
    },
    start_date:{
        type: Date,
        default: new Date(),
        required: true,        
    },
    end_date:{
        type: Date,
        default: new Date(),
        required: true,
    },    
});

module.exports = new model('Log', logSchema);