const { Schema, model } = require('mongoose');

const accountSchema = new Schema({
    id: {
        type: Number,
        unique: true,
        required: true
    },
    account: {
        type: String,
        unique: true,
        required: true
    },
    client: {
        type: String,
        required: true,
    },
    opresponsible: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
    teams: [{
        type: Schema.Types.ObjectId,
        ref: "Team"
    }]
});

module.exports = new model('Account', accountSchema);