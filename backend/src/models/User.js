const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    id: {
        type: Number,
        required: true,
        unique: true
    },    
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true,
        min: 6,
        max: 12,
    },
    roles: [{
        type: Schema.Types.ObjectId,
        ref: "Role"
    }],
    courses: [{
        type: Schema.Types.ObjectId,
        ref: "Course"
    }],    
    team:{
        type: Schema.Types.ObjectId,
        ref: "Team"        
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    name: String
});

module.exports = new model('User', userSchema);