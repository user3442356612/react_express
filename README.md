### React / Express API using docker compose

Project example by Arturo Santana

### How to run

1.- docker-compose build<br/>
2.- docker-compose up<br/>
3.- Access by http://localhost:3000/login using following credentials<br/>
user: admin<br/>
password: 123456
