import React, { useState } from "react";
import dataMag from '../asset/data.png'
import './MainPage.scss';


/**
    * Renders the main page of the project
    * @function
    */
function MainPage() {
    const [content, setContent] = useState("");
    const [data, setData] = useState([]);
  
    return (
        <>
            <div className="welcome">
                <div className="wc1">
                Client management
                </div>
                <div className="wc2">
                    Central software for manage sensitive records
                </div>
            </div>
            <div className="cardContainer">
                <div className="card">
                    <div className="cardtitle">Manage</div>
                    <div className="cardcontent">Select, add, remove, modify and delete data according with your Organitation Unit specification</div>
                </div>
                <div className="card">
                    <div className="cardtitle">Audit</div>
                    <div className="cardcontent">Analize and review records for the current period, for accomplish consistense, clean, standarized data</div>
                </div>
                <div className="card">
                    <div className="cardtitle">Browse</div>
                    <div className="cardcontent">Review the whole data stored by periods of time, from historical to current for any relevant finding requeriment</div>
                </div>
            </div>

            <div className="desc">
                <div>
                    <h3 className="text1">Client management</h3>
                    <p className="text2" >Client management enables companies to have a 360 view of their business, in order to make informed and intelligent decisions. It also allows to improve the experience of customers and potentials achieving personalized and satisfactory experiences.</p>
                    <button className="button-front-plain">Learn more</button>
                </div>

                <div style={{  width: '400px' }}>
                    <img height="300px" width="300px" src={dataMag} />
                </div>
            </div>         
        </>
    );
}

export default MainPage;
