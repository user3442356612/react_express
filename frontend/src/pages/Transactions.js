import React, { useMemo, useState, useEffect } from "react";
import Table from "../components/Table";
import axios from 'axios';
import Button from '../components/Button'
import './Transactions.scss'

/**
    * Renders the Transactions page, that shows example records from API for create/update data
    * @function
    * @property {array} dataTable Data obtained from API to React table
    * @property {int} lastId Last id generated for rows in table
    * @property {boolean} updated Flag for API data updates
    * @property {json} emptyRecord template for create a new empty record
    * @property {array} columns Columns headers for React table
    * @property {function} insert Add a new row in UI table for add new record in database
    * @property {function} save Save the data in table using API post
    */
function Transactions() {

  const [dataTable, setDataTable] = useState([]);
  const [lastId, setLastId] = useState(0);
  const [updated, setUpdated] = useState(false);
  const emptyRecord = {
    'account_amount': 0,
    'account_type': "",
    'category': "",
    'client': "",
    'credit_aplicable': false,
    'id': 1000,
    'score': 0
  };

  
  const columns = useMemo(
    () => [
      {
        Header: "Credit score for individuals",
        columns: [
          {
            Header: "Client",
            accessor: "client"
          },
          {
            Header: "Category",
            accessor: "category"
          },
          {
            Header: "Account type",
            accessor: "account_type"
          },
          {
            Header: "Account amount",
            accessor: "account_amount"
          },
          {
            Header: "Credit aplicable",
            accessor: "credit_aplicable"
          }
          ,
          {
            Header: "Expand",
            accessor: "delete"
          }
        ]
      }
    ],
    []
  );
  useEffect(() => {
    axios.get("http://localhost:8080/api/creditclient/all").then(data => {
      setDataTable(
        data.data.map(row => {
          row.credit_aplicable = row.credit_aplicable.toString();
          row.delete = "";
          return row;
        }))
      setLastId(data.data[data.data.length - 1].id);
    });

  }, []);

  const insert = () => {
    setDataTable([...dataTable, { ...emptyRecord, id: lastId }]);
    setLastId(lastId + 1);
  };

  const save = () => {
    var dataTemp = dataTable.map(row => {
      return { ...row, credit_aplicable: Boolean(row.credit_aplicable) }
    }
    );

    axios.post("http://localhost:8080/api/creditclient/save", dataTemp).then(data => {
      setDataTable(
        data.data.map(row => {
          row.credit_aplicable = row.credit_aplicable.toString();
          return row;
        }))
      setUpdated(true);
    })

  };

  return (
    <div >
      <div className="topPanel" >
        <h1 className="insidePanel" >Credit score for individuals</h1>
        The organitation unit is on charge of manage, measure, propose and archive credit metrics and scores for the bank clients, specifically for individuals.
      </div>
      <div className="dataTable">
        <Table columns={columns} dataT={dataTable} setDataTable={setDataTable} />
        <Button small={true} onClick={insert} text="Insert"/>
        <Button small={true} onClick={save} text="Save changes"/>
        {updated ? <p>Data changes were saved successfully!</p> : null}
      </div>
    </div>
  );
}

Transactions.propTypes = {
  
};

Transactions.defaultProps = {
 
};

export default Transactions;
