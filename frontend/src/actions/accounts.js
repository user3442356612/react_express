import { 
  ACCOUNT_CREATE_SUCCESS,
  ACCOUNT_CREATE_FAIL,
  ACCOUNT_READ_SUCCESS,
  ACCOUNT_READ_FAIL,
  ACCOUNT_UPDATE_SUCCESS,
  ACCOUNT_UPDATE_FAIL,
  ACCOUNT_DELETE_SUCCESS,
  ACCOUNT_DELETE_FAIL
} from "./types";

import AccountService from "../services/account.service";

export const addAccount = (user, category, unit, audit, upload, edit) => (dispatch) => {
  return AccountService.postAccount(user, category, unit, audit, upload, edit).then(
    (response) => {
      dispatch({
        type: ACCOUNT_CREATE_SUCCESS,
      });

      return Promise.resolve(response.data.result);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ACCOUNT_CREATE_FAIL,
      });

  

      return Promise.reject();
    }
  );
};

export const addAccounts = (data) => (dispatch) => {
  return AccountService.postAccounts(data).then(
    (response) => {
      dispatch({
        type: ACCOUNT_CREATE_SUCCESS,
      });

      return Promise.resolve(response.data.result);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ACCOUNT_CREATE_FAIL,
      });
      return Promise.reject();
    }
  );
};

export const getAllAccounts = () => (dispatch) => {
  return AccountService.getAccounts().then(
    (response) => {
      dispatch({
        type: ACCOUNT_READ_SUCCESS,
      });
      console.log(response)
      return Promise.resolve(response.data);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ACCOUNT_READ_FAIL,
      });

  

      return Promise.reject();
    }
  );
};

export const getAccountById = (id) => (dispatch) => {
  return AccountService.getAccountById(id).then(
    (response) => {
      dispatch({
        type: ACCOUNT_READ_SUCCESS,
      });

      return Promise.resolve(response.data.result);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ACCOUNT_READ_FAIL,
      });

  

      return Promise.reject();
    }
  );
};

export const updateAccountById = (id) => (dispatch) => {
  return AccountService.updateAccountById(id).then(
    (response) => {
      dispatch({
        type: ACCOUNT_UPDATE_SUCCESS,
      });

      return Promise.resolve(response.data.result);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ACCOUNT_UPDATE_FAIL,
      });
      return Promise.reject();
    }
  );
};

export const deleteAccount = (id) => (dispatch) => {
  return AccountService.deleteAccount(id).then(
    (response) => {
      dispatch({
        type: ACCOUNT_DELETE_SUCCESS,
      });

      return Promise.resolve(response.data.result);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: ACCOUNT_DELETE_FAIL,
      });
      return Promise.reject();
    }
  );
};