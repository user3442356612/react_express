import { 
  USER_CREATE_SUCCESS,
  USER_CREATE_FAIL,
  USER_READ_SUCCESS,
  USER_READ_FAIL,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_FAIL,
  USER_DELETE_SUCCESS,
  USER_DELETE_FAIL
} from "./types";

import UserService from "../services/user.service";

export const addUser = (id,username,password,roles,team) => (dispatch) => {
  return UserService.postUser(id,username,password,roles,team).then(
    (response) => {
      console.log(response)
      dispatch({
        type: USER_CREATE_SUCCESS,
      });

      return Promise.resolve(response.data);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USER_CREATE_FAIL,
      });

  

      return Promise.reject();
    }
  );
};

export const addUsers = (data) => (dispatch) => {
  return UserService.postUsers(data).then(
    (response) => {
      console.log(response)
      dispatch({
        type: USER_CREATE_SUCCESS,
      });

      return Promise.resolve(response.data);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USER_CREATE_FAIL,
      });

  

      return Promise.reject();
    }
  );
};

export const getUsers = () => (dispatch) => {
  return UserService.getUsers().then(
    (response) => {
      dispatch({
        type: USER_READ_SUCCESS,
      });

      return Promise.resolve(response.data);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USER_READ_FAIL,
      });

  

      return Promise.reject();
    }
  );
};

export const getUserById = (id) => (dispatch) => {
  return UserService.getUserById(id).then(
    (response) => {
      dispatch({
        type: USER_READ_SUCCESS,
      });

      return Promise.resolve(response.data.result);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USER_READ_FAIL,
      });

  

      return Promise.reject();
    }
  );
};

export const updateUserById = (id,username,password,roles,team) => (dispatch) => {
  return UserService.updateUserById(id,username,password,roles,team).then(
    (response) => {
      dispatch({
        type: USER_UPDATE_SUCCESS,
      });
      return Promise.resolve(response.data);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USER_UPDATE_FAIL,
      });
      return Promise.reject();
    }
  );
};

export const deleteUser = (id) => (dispatch) => {
  return UserService.deleteUser(id).then(
    (response) => {
      dispatch({
        type: USER_DELETE_SUCCESS,
      });

      return Promise.resolve(response.data.result);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: USER_DELETE_FAIL,
      });
      return Promise.reject();
    }
  );
};