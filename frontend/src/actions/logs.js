import { 
  LOG_CREATE_SUCCESS,
  LOG_CREATE_FAIL,
  LOG_READ_SUCCESS,
  LOG_READ_FAIL,
  LOG_UPDATE_SUCCESS,
  LOG_UPDATE_FAIL,
  LOG_DELETE_SUCCESS,
  LOG_DELETE_FAIL
} from "./types";

import LogService from "../services/log.service";

export const addLog = (id,user,team,start_date,end_date) => (dispatch) => {
  return LogService.postLog(id,user,team,start_date,end_date).then(
    (response) => {
      console.log(response)
      dispatch({
        type: LOG_CREATE_SUCCESS,
      });

      return Promise.resolve(response.data);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: LOG_CREATE_FAIL,
      });

  

      return Promise.reject();
    }
  );
};


export const addLogs = (data) => (dispatch) => {
  return LogService.postLogs(data).then(
    (response) => {
      console.log(response)
      dispatch({
        type: LOG_CREATE_SUCCESS,
      });

      return Promise.resolve(response.data);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: LOG_CREATE_FAIL,
      });
      return Promise.reject();
    }
  );
};

export const getLogs = () => (dispatch) => {
  return LogService.getLogs().then(
    (response) => {
      dispatch({
        type: LOG_READ_SUCCESS,
      });

      return Promise.resolve(response.data);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: LOG_READ_FAIL,
      });

  

      return Promise.reject();
    }
  );
};

export const getLogById = (id) => (dispatch) => {
  return LogService.getLogById(id).then(
    (response) => {
      dispatch({
        type: LOG_READ_SUCCESS,
      });

      return Promise.resolve(response.data.result);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: LOG_READ_FAIL,
      });

  

      return Promise.reject();
    }
  );
};

export const updateLogById = (id,user,team,start_date,end_date) => (dispatch) => {
  return LogService.updateLogById(id,user,team,start_date,end_date).then(
    (response) => {
      dispatch({
        type: LOG_UPDATE_SUCCESS,
      });
      return Promise.resolve(response.data);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: LOG_UPDATE_FAIL,
      });
      return Promise.reject();
    }
  );
};

export const deleteLog = (id) => (dispatch) => {
  return LogService.deleteLog(id).then(
    (response) => {
      dispatch({
        type: LOG_DELETE_SUCCESS,
      });

      return Promise.resolve(response.data.result);
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: LOG_DELETE_FAIL,
      });
      return Promise.reject();
    }
  );
};