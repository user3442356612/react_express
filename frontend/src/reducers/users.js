import {
  USER_CREATE_SUCCESS,
  USER_CREATE_FAIL,
  USER_READ_SUCCESS,
  USER_READ_FAIL,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_FAIL,
  USER_DELETE_SUCCESS,
  USER_DELETE_FAIL
} from "../actions/types";

const user = JSON.parse(localStorage.getItem("user"));

const initialState = {};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case USER_CREATE_SUCCESS:
      return {
        ...state,
        userCreate: payload,
      };
    case USER_CREATE_FAIL:
      return {
        ...state,
        userCreate: payload,
      };
    case USER_READ_SUCCESS:
      return {
        ...state,
        userRead: payload,
      };
    case USER_READ_FAIL:
      return {
        ...state,
        userRead: payload,
      };
    case USER_UPDATE_SUCCESS:
      return {
        ...state,
        userUpdate: payload,
      };
    case USER_UPDATE_FAIL:
      return {
        ...state,
        userUpdate: payload,
      };
    case USER_DELETE_SUCCESS:
      return {
        ...state,
        userDelete: payload,
      };
    case USER_DELETE_FAIL:
      return {
        ...state,
        userDelete: payload,
      };
    default:
      return state;
  }
}
