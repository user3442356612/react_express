import { combineReducers } from "redux";
import auth from "./auth";
import account from "./account";
import users from "./users";

export default combineReducers({
  auth,
  account,
  users
});
