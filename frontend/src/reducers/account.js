import {
  ACCOUNT_CREATE_SUCCESS,
  ACCOUNT_CREATE_FAIL,
  ACCOUNT_READ_SUCCESS,
  ACCOUNT_READ_FAIL,
  ACCOUNT_UPDATE_SUCCESS,
  ACCOUNT_UPDATE_FAIL,
  ACCOUNT_DELETE_SUCCESS,
  ACCOUNT_DELETE_FAIL
} from "../actions/types";

const user = JSON.parse(localStorage.getItem("user"));

const initialState = {};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case ACCOUNT_CREATE_SUCCESS:
      return {
        ...state,
        accountCreate: payload,
      };
    case ACCOUNT_CREATE_FAIL:
      return {
        ...state,
        accountCreate: payload,
      };
    case ACCOUNT_READ_SUCCESS:
      return {
        ...state,
        accountRead: payload,
      };
    case ACCOUNT_READ_FAIL:
      return {
        ...state,
        accountRead: payload,
      };
    case ACCOUNT_UPDATE_SUCCESS:
      return {
        ...state,
        accountUpdate: payload,
      };
    case ACCOUNT_UPDATE_FAIL:
      return {
        ...state,
        accountUpdate: payload,
      };
    case ACCOUNT_DELETE_SUCCESS:
      return {
        ...state,
        accountDelete: payload,
      };
    case ACCOUNT_DELETE_FAIL:
      return {
        ...state,
        accountDelete: payload,
      };
    default:
      return state;
  }
}
