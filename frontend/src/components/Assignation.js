import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Select from "react-validation/build/select";
import { isEmail } from "validator";
import { register } from "../actions/auth";
import Button from './Button'
import './Register.scss'

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Field required
      </div>
    );
  }
};

const validEmail = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        Email not valid, enter a valid email
      </div>
    );
  }
};

const vusername = (value) => {
  if(!value || !!parseInt(value[0]) || (value.length < 3 || value.length > 20))
  return (
    <div className="alert alert-danger" role="alert">
      {!value && <span>This field is required<br /></span>}
      {!!parseInt(value[0]) && <span>First character can't be number<br /></span>}
      {(value.length < 3 || value.length > 20) && <span>Username must be between 3 and 20 characters<br /></span>}
    </div>
  );

};

const vpassword = (value) => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
       <span>Password must be between 6 and 40 characters</span>
      </div>
    );
  }
};

const Assignation = () => {
  const form = useRef();
  const checkBtn = useRef();
  const [user, setUser] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [successful, setSuccessful] = useState(false);
  const dispatch = useDispatch();

  const onChangeUser= (e) => {
    setUser(e.target.value);
  };    

  const onChangeStartDate = (e) => {
    setStartDate(e.target.value);
  };  

  const onChangeEndDate = (e) => {
    setEndDate(e.target.value);
  };  






  const handleAssignation = (e) => {
    e.preventDefault();

    setSuccessful(false);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      /*dispatch(register(username, name, english, email, password))
        .then(() => {
          setSuccessful(true);
        })
        .catch(() => {
          setSuccessful(false);
        });
    }*/
  };
}

  return (
    <div >
      <div >
        <div className="topPanelReg" >
          <h1 style={{ 'margin': '0 0 10px 0' }} >Assignations page</h1>
          The following page would register a new assignation
        </div>

        <Form onSubmit={handleAssignation} ref={form}>
          {!successful && (
            <div className='form' >
              <div className="form-group">
                <label htmlFor="user">User</label>
                <Select
                  type="select"
                  className="form-control"
                  name="user"
                  value={user}
                  onChange={onChangeUser}
                  validations={[required]}
                >
                  <option value="">Select level</option>
                  <option value="Beginner">Beginner level</option>
                  <option value="Intermediate">Intermediate level</option>
                  <option value="UperIntermediate">Uper Intermediate level</option>
                  <option value="Advanced">Advanced level</option>
                  <option value="C2">C2 level</option>
                </Select>
                <label htmlFor="startDate">Start date</label>
                <Input
                  type="text"
                  className="form-control"
                  name="startDate"
                  value={startDate}
                  onChange={onChangeStartDate}
                  validations={[required]}
                /> 
                <label htmlFor="endDate">End date</label>
                <Input
                  type="text"
                  className="form-control"
                  name="endDate"
                  value={endDate}
                  onChange={onChangeEndDate}
                  validations={[required]}
                />
                
              </div>

              <div className="form-group">
                <Button text="Sign Up" />
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
        {successful && <span>Account added!</span>}
      </div>
    </div>
  );
};

export default Assignation;