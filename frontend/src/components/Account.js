import React, { useState, useRef, useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from 'react-router-dom';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Button from './Button'
import Table from './Table'
import { login, logout } from "../actions/auth";
import { addAccount, addAccounts, updateAccountById, getAllAccounts, deleteAccount } from "../actions/accounts";

import './Login.scss'

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const Account = (props) => {
  const form = useRef();
  const formUpdate = useRef();
  const checkBtn = useRef();
  const checkBtnUpdate = useRef();

  const [apiAccounts, setApiAccounts] = useState([]);
  const [dataTable, setDataTable] = useState([]);
  const [accountId, setAccountId] = useState();
  const [account, setAccount] = useState("");
  const [opresponsible, setOpResponsible] = useState("");
  const [client, setClient] = useState("");
  const [teams, setTeams] = useState();

  const [accountIdUpdate, setAccountIdUpdate] = useState();
  const [accountUpdate, setAccountUpdate] = useState("");
  const [opresponsibleUpdate, setOpResponsibleUpdate] = useState("");
  const [clientUpdate, setClientUpdate] = useState("");
  const [teamsUpdate, setTeamsUpdate] = useState("");

  const [logged, setLogged] = useState(false);
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState("");
  const [error, setError] = useState("");
  const { isLoggedIn } = useSelector(state => state.auth);

  const columns = useMemo(
    () => [
      {
        // first group - TV Show
        Header: "Accounts table",
        // First group columns
        columns: [
          {
            Header: "Id",
            accessor: "id"
          },
          {
            Header: "Account name",
            accessor: "account"
          },
          {
            Header: "Client",
            accessor: "client"
          },
          {
            Header: "Operations responsible",
            accessor: "opresponsible"
          },
          {
            Header: "Teams",
            accessor: "teams"
          },
          {
            Header: "Delete",
            accessor: "delete"
          },          
        ]
      },

    ],
    []
  );



  useEffect(() => {
    dispatch(getAllAccounts())
      .then((response) => {
        let res = response.map(item =>{
          item.teams = Array.isArray( item.teams)?(item.teams.map(item => item.name)).join(","):item.teams;
          if(item.opresponsible) item.opresponsible =  item.opresponsible.username;
          console.log(item)
        return item;})
        setApiAccounts(response);
        setDataTable(response);
        // props.history.push("/profile");
        //window.location.reload();
      })
      .catch((err) => {
        setLoading(false);
        setError(err);
        console.log(err)
      });
  }, [])

  /**useEffect(()=>{
    dispatch(logout());

  },[]) */

  const dispatch = useDispatch();

  const onChangeAccountId = (e) => {
    const ids = e.target.value;
    setAccountId(ids);
  };


  const onChangeAccount = (e) => {
    setAccount(e.target.value);
  };

  const onChangeOpResponsible = (e) => {
    const accountRoles = e.target.value;
    setOpResponsible(accountRoles);
  };

  const onChangeClient = (e) => {
    setClient(e.target.value);
  };

  const onChangeTeams = (e) => {
    const accountTeams = e.target.value;
    setTeams(accountTeams);
  };

  const onChangeAccountIdUpdate = (e) => {
    const ids = e.target.value;
    setAccountIdUpdate(ids);
  };

  const onChangeAccountUpdate = (e) => {
    setAccountUpdate(e.target.value);
  };

  const onChangeOpResponsibleUpdate = (e) => {
    const accountRoles = e.target.value;
    setOpResponsibleUpdate(accountRoles);
  };

  const onChangeClientUpdate = (e) => {
    setClientUpdate(e.target.value);
  };

  const onChangeTeamsUpdate = (e) => {
    const accountTeams = e.target.value;
    setTeamsUpdate(accountTeams);
  };

  const handleInsert = (e) => {

    e.preventDefault();
    setLoading(true);
    form.current.validateAll();
    if (checkBtn.current.context._errors.length === 0) {
      dispatch(addAccount(accountId, account, opresponsible, client, teams))
        .then(() => {
          // props.history.push("/profile");
          //window.location.reload();
          dispatch(getAllAccounts())
            .then((response) => {
              setApiAccounts(response);
              setDataTable([...response]);

              // props.history.push("/profile");
              //window.location.reload();
            })
            .catch((err) => {
              setLoading(false);
              setError(err);
            });
        })
        .catch((err) => {
          setLoading(false);
          setError(false);
        });
    } else {
      setLoading(false);
    }
  };

  const handleUpdate = async (e) => {

    e.preventDefault();
    setLoading(true);
    formUpdate.current.validateAll();
    if (checkBtnUpdate.current.context._errors.length === 0) {
      let upt = await dispatch(updateAccountById(accountIdUpdate, accountUpdate, opresponsibleUpdate, clientUpdate, teamsUpdate))

      // props.history.push("/profile");
      //window.location.reload();
      let getU = await dispatch(getAllAccounts())

      setDataTable(getU.map(row => { return { ...row } }));
      // props.history.push("/profile");
      //window.location.reload();

    } else {
      setLoading(false);
    }
  };

  const onSelectRecord = (data) => {
    console.log(data);
    if (data.id) setAccountIdUpdate(data.id); else setAccountIdUpdate("");
    if (data.account) setAccountUpdate(data.account); else setAccountUpdate("");
    if (data.client) setClientUpdate(data.client); else setClientUpdate("");
    if (data.opresponsible) setOpResponsibleUpdate(data.opresponsible); else setOpResponsibleUpdate("");
    if (data.teams) setTeamsUpdate(data.teams); else setTeamsUpdate("");
  };

  const saveChanges = () => {
    dispatch(addAccounts(dataTable))
      .then(() => {
        dispatch(getAllAccounts())
          .then((response) => {
            setApiAccounts(response);
            setDataTable([...response]);
          })
          .catch((err) => {
            setLoading(false);
            setError(err);
          });
      })
      .catch((err) => {
        setLoading(false);
        setError(false);
      });
  };


  const onDeleteRecord = async (data) => {
    console.log(data);
    if (apiAccounts.filter(act => act.id == data.id).length > 0) {
      dispatch(deleteAccount(data.id))
        .then(() => {
          // props.history.push("/profile");
          //window.location.reload();
          dispatch(getAllAccounts())
            .then((response) => {
              setApiAccounts(response);
              setDataTable(response);
              // props.history.push("/profile");
              //window.location.reload();
            })
            .catch((err) => {
              setLoading(false);
              setError(err);
            });
        })
        .catch((err) => {
          setLoading(false);
          setError(false);
        });
    }
    else
    {
      await setDataTable(old =>{
        return new Array(...old.filter(row => parseInt(row.id) != data.id))
      });
    }
  };  

  const addRow = async () => {
   
    await setDataTable(old => [...old, {
      id: old[old.length-1].id+1,
      account: '',
      opresponsible: '',
      client: '',
      teams: '',
    }]);
  };

  const updateMyData = (rowIndex, columnId, value) => {

    setDataTable(old => {
      return old.map((row, index) => {
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnId]: value,
          }
        }
        return row
      })
    });

  }

  

  return (

    <div className="login">
      <div className="topPanelLog" >
        <h1 style={{ 'margin': '0 0 10px 0' }} >Accounts page</h1>
        Page for create, read, update and delete accounts
      </div>
      <div style={{ 'padding': '30px','display': 'flex',
          'justify-content': 'center','flex-direction':'column' }}  >
        <div style={{
          'display': 'flex',
          'justify-content': 'center'
        }}>
        <Table columns={columns} dataT={dataTable} updateMyData={updateMyData}  onDeleteRecord={onDeleteRecord} onSelectRecord={onSelectRecord} />
        </div>
        <div style={{'padding': '30px', display: "flex",
          'justify-content': 'center', 'gap':'30px' }}>
          <Button onClick={addRow} text="Add account" />
          <Button onClick={saveChanges} text="Save changes" />
        </div>
      </div>
    </div>
  );
};

export default Account;


/*<div style={{ display: "flex" }}>
          <Form className="form" onSubmit={handleInsert} ref={form}>
            <div className="form-group">
              <label htmlFor="accountId">Id</label>
              <Input
                type="number"
                className="form-control"
                name="accountId"
                value={accountId}
                onChange={onChangeAccountId}
                validations={[required]}
              />
              <label htmlFor="account">Account name</label>
              <Input
                type="text"
                className="form-control"
                name="account"
                value={account}
                onChange={onChangeAccount}
                validations={[required]}
              />

              <label htmlFor="client">Client</label>
              <Input
                type="text"
                className="form-control"
                name="client"
                value={client}
                onChange={onChangeClient}
                validations={[required]}
              />
              <label htmlFor="opresponsible">Op Responsible</label>
              <Input
                type="text"
                className="form-control"
                name="opresponsible"
                value={opresponsible}
                onChange={onChangeOpResponsible}
                validations={[required]}
              />
              <label htmlFor="teams">Teams</label>
              <Input
                type="text"
                className="form-control"
                name="teams"
                value={teams}
                onChange={onChangeTeams}
                validations={[required]}
              />
            </div>
            <div className="form-group">
              <Button text="Add account" />
            </div>
            <CheckButton style={{ display: "none" }} ref={checkBtn} />
          </Form>

          <Form className="form" onSubmit={handleUpdate} ref={formUpdate}>
            <div className="form-group">
              <label htmlFor="accountIdUpdate">Id</label>
              <Input
                type="number"
                className="form-control"
                name="accountIdUpdate"
                value={accountIdUpdate}
                onChange={onChangeAccountIdUpdate}
                validations={[required]}
              />
              <label htmlFor="accountUpdate">Account name</label>
              <Input
                type="text"
                className="form-control"
                name="accountUpdate"
                value={accountUpdate}
                onChange={onChangeAccountUpdate}
                validations={[required]}
              />


              <label htmlFor="clientUpdate">Client</label>
              <Input
                type="text"
                className="form-control"
                name="clientUpdate"
                value={clientUpdate}
                onChange={onChangeClientUpdate}
                validations={[required]}
              />
              <label htmlFor="opresponsibleUpdate">Op Responsible</label>
              <Input
                type="text"
                className="form-control"
                name="opresponsibleUpdate"
                value={opresponsibleUpdate}
                onChange={onChangeOpResponsibleUpdate}
                validations={[required]}
              />
              <label htmlFor="teamsUpdate">Teams</label>
              <Input
                type="text"
                className="form-control"
                name="teamsUpdate"
                value={teamsUpdate}
                onChange={onChangeTeamsUpdate}
                validations={[required]}
              />
            </div>
            <div className="form-group">
              <Button text="Update account" />
            </div>
            <CheckButton style={{ display: "none" }} ref={checkBtnUpdate} />
          </Form>
        </div>*/