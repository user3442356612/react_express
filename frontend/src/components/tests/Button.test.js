import React from 'react';
import {shallow} from 'enzyme';
import Button from '../Button';
test('CheckboxWithLabel changes the text after click', () => {
  const checkbox = shallow(
    <Button labelOn="On" labelOff="Off" />
  );
  expect(checkbox.text()).toEqual('Off');
  checkbox.find('input').simulate('change');
  expect(checkbox.text()).toEqual('On');
});