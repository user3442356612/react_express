import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

const Button = (props) => {
    return (
      <button {...props} className={!props.small?"button-front":"button-front-sm"}>{props.text}</button>
    );
  
}

Button.propTypes = {
  
};

Button.defaultProps = {
 
};

export default Button;