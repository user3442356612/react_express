import React from 'react';
import MainPage from '../../pages/MainPage';

export default {
  title: 'Pages/MainPage',
  component: MainPage,
  argTypes: {
    label: {
      control: { type: 'text' }
    }
  }
}
export const Primary = (args) => <MainPage  text={args.label} />;