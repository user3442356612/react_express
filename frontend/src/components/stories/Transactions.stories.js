import React from 'react';
import withMock from 'storybook-addon-mock';
import Transactions from '../../pages/Transactions';

export default {
  title: 'Pages/Transactions',
  component: Transactions,
  decorators: [withMock],

}
const Template = (args) => <Transactions {...args} />;
export const Default = Template.bind({});
Default.parameters = {
  mockData: [
      {
          url: 'http://localhost:8080/api/creditclient/all',
          method: 'GET',
          status: 200,
          response: {
              data: [{client:"",category:"",account_type:"",account_amount:"",credit_aplicable:""}],
          },
      },
  ],
};