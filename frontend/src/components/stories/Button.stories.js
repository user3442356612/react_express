import React from 'react';

import  Button2 from '../Button';

export default {
  title: 'Components/Button',
  component: Button2,
  argTypes: {
    label: {
      control: { type: 'text' }
    }
  }
}
export const Primary = (args) => <Button  text={args.label} />;