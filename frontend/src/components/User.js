import React, { useState, useRef, useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from 'react-router-dom';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Button from './Button'
import Table from './Table'
import { login, logout } from "../actions/auth";
import { addUser, addUsers, updateUserById, getUsers, deleteUser } from "../actions/users";

import './Login.scss'

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const User = (props) => {
  const form = useRef();
  const formUpdate = useRef();
  const checkBtn = useRef();
  const checkBtnUpdate = useRef();

  const [apiUsers, setApiUsers] = useState([]);
  const [dataTable, setDataTable] = useState([]);
  const [userId, setUserId] = useState();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [userRoles, setUserRoles] = useState();
  const [userTeam, setUserTeam] = useState();

  const [userIdUpdate, setUserIdUpdate] = useState();
  const [usernameUpdate, setUsernameUpdate] = useState("");
  const [passwordUpdate, setPasswordUpdate] = useState("");
  const [userRolesUpdate, setUserRolesUpdate] = useState();
  const [userTeamUpdate, setUserTeamUpdate] = useState();

  const [logged, setLogged] = useState(false);
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState("");
  const [error, setError] = useState("");
  const { isLoggedIn } = useSelector(state => state.auth);

  const columns = useMemo(
    () => [
      {
        // first group - TV Show
        Header: "Users table",
        // First group columns
        columns: [
          {
            Header: "Id",
            accessor: "id"
          },
          {
            Header: "Username",
            accessor: "username"
          },
          {
            Header: "Password",
            accessor: "password"
          },
          {
            Header: "Roles",
            accessor: "roles"
          },
          {
            Header: "Team",
            accessor: "team"
          },
          {
            Header: "Created at",
            accessor: "createdAt"
          },
          {
            Header: "Delete",
            accessor: "delete"
          },
        ]
      },

    ],
    []
  );



  useEffect(() => {
    dispatch(getUsers())
      .then((response) => {

        let res = response.map(item => {
          item.roles = Array.isArray(item.roles) ? (item.roles.map(item => item.name)).join(",") : item.roles;

          console.log(item)
          return item;
        }
        );
        console.log(res)
        setApiUsers(response);
        setDataTable(response);
      })
      .catch((err) => {
        setLoading(false);
        setError(err);
      });
  }, [])

  /**useEffect(()=>{
    dispatch(logout());

  },[]) */

  const dispatch = useDispatch();

  const onChangeUserId = (e) => {
    const ids = e.target.value;
    setUserId(ids);
  };


  const onChangeUsername = (e) => {
    setUsername(e.target.value);
  };

  const onChangeUserRoles = (e) => {
    const userRoles = e.target.value;
    setUserRoles(userRoles);
  };

  const addRow = async () => {
   
    await setDataTable(old => [...old, {
      id: old[old.length-1].id+1,
      username: '',
      password: '',
      roles: '',
      courses: [],
      createdAt: new Date().toUTCString(),
    }]);
    console.log(dataTable)
  };

  const onChangeUserTeam = (e) => {
    const userTeam = e.target.value;
    setUserTeam(userTeam);
  };

  const onChangePassword = (e) => {
    setPassword(e.target.value);
  };


  const onChangeUserIdUpdate = (e) => {
    const ids = e.target.value;
    setUserIdUpdate(ids);
  };


  const onChangeUsernameUpdate = (e) => {
    setUsernameUpdate(e.target.value);
  };

  const onChangeUserRolesUpdate = (e) => {
    const userRolesUpdate = e.target.value;
    setUserRolesUpdate(userRolesUpdate);
  };

  const onChangeUserTeamUpdate = (e) => {
    const userTeamUpdate = e.target.value;
    setUserTeamUpdate(userTeamUpdate);
  };

  const onChangePasswordUpdate = (e) => {
    setPasswordUpdate(e.target.value);
  };

  const handleInsert = (e) => {

    e.preventDefault();
    setLoading(true);
    form.current.validateAll();
    if (checkBtn.current.context._errors.length === 0) {
      dispatch(addUser(userId, username, password, [userRoles], [userTeam]))
        .then(() => {
          // props.history.push("/profile");
          //window.location.reload();
          dispatch(getUsers())
            .then((response) => {
              setApiUsers(response);
              setDataTable([...response]);

              // props.history.push("/profile");
              //window.location.reload();
            })
            .catch((err) => {
              setLoading(false);
              setError(err);
            });
        })
        .catch((err) => {
          setLoading(false);
          setError(false);
        });
    } else {
      setLoading(false);
    }
  };

  const handleUpdate = async (e) => {

    e.preventDefault();
    setLoading(true);
    formUpdate.current.validateAll();
    if (checkBtnUpdate.current.context._errors.length === 0) {
      let upt = await dispatch(updateUserById(userIdUpdate, usernameUpdate, passwordUpdate, [userRolesUpdate], [userTeamUpdate]))

      // props.history.push("/profile");
      //window.location.reload();
      let getU = await dispatch(getUsers())

      setDataTable(getU.map(row => { return { ...row } }));
      // props.history.push("/profile");
      //window.location.reload();

    } else {
      setLoading(false);
    }
  };

  const onSelectRecord = (data) => {
    console.log(data);
    if (data.id) setUserIdUpdate(data.id); else setUserIdUpdate("");
    if (data.username) setUsernameUpdate(data.username); else setUsernameUpdate("");
    if (data.roles) setUserRolesUpdate(data.roles); else setUserRolesUpdate("");
    if (data.team) setUserTeamUpdate(data.team); else setUserTeamUpdate("");
    if (data.password) setPasswordUpdate(data.password); else setPasswordUpdate("");



  };

  const onDeleteRecord = async (data) => {
    console.log(data);
    if (apiUsers.filter(user => user.id == data.id).length > 0) {
      dispatch(deleteUser(data.id))
        .then(() => {
          // props.history.push("/profile");
          //window.location.reload();
          dispatch(getUsers())
            .then((response) => {
              setApiUsers(response);
              setDataTable(response);
              // props.history.push("/profile");
              //window.location.reload();
            })
            .catch((err) => {
              setLoading(false);
              setError(err);
            });
        })
        .catch((err) => {
          setLoading(false);
          setError(false);
        });
    }
    else
    {
      await setDataTable(old =>{
        return new Array(...old.filter(row => parseInt(row.id) != data.id))
      });
    }

  };

  const saveChanges = () => {
 
    dispatch(addUsers(dataTable))
      .then(() => {
        // props.history.push("/profile");
        //window.location.reload();
        dispatch(getUsers())
          .then((response) => {
            setApiUsers(response);
            setDataTable([...response]);

            // props.history.push("/profile");
            //window.location.reload();
          })
          .catch((err) => {
            setLoading(false);
            setError(err);
          });
      })
      .catch((err) => {
        setLoading(false);
        setError(false);
      });

  };

  const updateMyData = (rowIndex, columnId, value) => {

    setDataTable(old => {
      return old.map((row, index) => {
        if (index === rowIndex) {
          return {
            ...old[rowIndex],
            [columnId]: value,
          }
        }
        return row
      })
    });

  }

  return (

    <div className="login">
      <div className="topPanelLog" >
        <h1 style={{ 'margin': '0 0 10px 0' }} >Users page</h1>
        Page for create, read, update and delete users
      </div>
      <div style={{
        'padding': '30px', 'display': 'flex',
        'justify-content': 'center', 'flex-direction': 'column'
      }}  >
        <div style={{
          'display': 'flex',
          'justify-content': 'center'
        }}>
          <Table columns={columns} dataT={dataTable} updateMyData={updateMyData} onDeleteRecord={onDeleteRecord} onSelectRecord={onSelectRecord} />
        </div>
        <div style={{
          'padding': '30px', display: "flex",
          'justify-content': 'center', 'gap': '30px'
        }}>
          <Button onClick={addRow} text="Add user" />
          <Button onClick={saveChanges} text="Save changes" />
        </div>
      </div>
    </div>
  );
};

export default User;


/*   <Form className="form" onSubmit={handleInsert} ref={form}>
          <div className="form-group">
            <label htmlFor="userId">Id</label>
            <Input
              type="number"
              className="form-control"
              name="userId"
              value={userId}
              onChange={onChangeUserId}
              validations={[required]}
            />
            <label htmlFor="username">Username</label>
            <Input
              type="text"
              className="form-control"
              name="username"
              value={username}
              onChange={onChangeUsername}
              validations={[required]}
            />

            <label htmlFor="password">Password</label>
            <Input
              type="password"
              className="form-control"
              name="password"
              value={password}
              onChange={onChangePassword}
              validations={[required]}
            />
            <label htmlFor="userRoles">Roles</label>
            <Input
              type="text"
              className="form-control"
              name="userRoles"
              value={userRoles}
              onChange={onChangeUserRoles}
              validations={[required]}
            />
            <label htmlFor="userTeam">Team</label>
            <Input
              type="text"
              className="form-control"
              name="userTeam"
              value={userTeam}
              onChange={onChangeUserTeam}
              validations={[required]}
            />
          </div>
          <div className="form-group">
            <Button text="Add user" />
          </div>
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>

        <Form className="form" onSubmit={handleUpdate} ref={formUpdate}>
          <div className="form-group">
            <label htmlFor="userIdUpdate">Id</label>
            <Input
              type="number"
              className="form-control"
              name="userIdUpdate"
              value={userIdUpdate}
              onChange={onChangeUserIdUpdate}
              validations={[required]}
            />
            <label htmlFor="usernameUpdate">Username</label>
            <Input
              type="text"
              className="form-control"
              name="usernameUpdate"
              value={usernameUpdate}
              onChange={onChangeUsernameUpdate}
              validations={[required]}
            />

            <label htmlFor="passwordUpdate">Password</label>
            <Input
              type="password"
              className="form-control"
              name="passwordUpdate"
              value={passwordUpdate}
              onChange={onChangePasswordUpdate}
              validations={[required]}
            />
            <label htmlFor="userRolesUpdate">Roles</label>
            <Input
              type="text"
              className="form-control"
              name="userRolesUpdate"
              value={userRolesUpdate}
              onChange={onChangeUserRolesUpdate}
              validations={[required]}
            />
            <label htmlFor="userTeamUpdate">Team</label>
            <Input
              type="text"
              className="form-control"
              name="userTeamUpdate"
              value={userTeamUpdate}
              onChange={onChangeUserTeamUpdate}
              validations={[required]}
            />
          </div>
          <div className="form-group">
            <Button text="Update user" />
          </div>
          <CheckButton style={{ display: "none" }} ref={checkBtnUpdate} />
        </Form>*/