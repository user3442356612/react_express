import React, { useState, useRef, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from 'react-router-dom';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Button from './Button'
import { login, logout } from "../actions/auth";
import { getUsers } from "../actions/users";


import './Login.scss'

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const Login = (props) => {
  const form = useRef();
  const checkBtn = useRef();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [logged, setLogged] = useState(false);
  const [loading, setLoading] = useState(false);
  const { isLoggedIn } = useSelector(state => state.auth);


  /*useEffect(()=>{
    dispatch(logout());

  },[])*/

  const dispatch = useDispatch();

  const onChangeUsername = (e) => {
    const username = e.target.value;
    setUsername(username);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  const handleLogin = (e) => {
    e.preventDefault();
    setLoading(true);
    form.current.validateAll();
    if (checkBtn.current.context._errors.length === 0) {
      dispatch(login(username, password))
        .then(() => {

          // props.history.push("/profile");
          //window.location.reload();
        })
        .catch(() => {
          setLoading(false);
        });
    } else {
      setLoading(false);
    }
  };


  const handleUsers = (e) => {
    dispatch(getUsers())
      .then((response) => {
        console.log("response.data")
        console.log(response)

      })
      .catch(() => {

      });
  }




  return (
   
    <div className="login">
     
      <div className="topPanelLog" >
        <h1 style={{ 'margin': '0 0 10px 0' }} >Login page</h1>
        The following service for access to your API operations
      </div>
      <Form className="form" onSubmit={handleLogin} ref={form}>
        <div className="form-group">
          <label htmlFor="username">Username</label>
          <Input
            type="text"
            className="form-control"
            name="username"
            value={username}
            onChange={onChangeUsername}
            validations={[required]}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <Input
            type="password"
            className="form-control"
            name="password"
            value={password}
            onChange={onChangePassword}
            validations={[required]}
          />
        </div>
        <div className="form-group">
          <Button text="Login" />
        </div>
        <CheckButton style={{ display: "none" }} ref={checkBtn} />
      </Form>
   
    </div>

  );
};

export default Login;
