import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import { requestAccess } from "../actions/request";
import Button from './Button'
import './Request.scss'

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const Request = () => {
  const { currentUser } = useSelector((state) => state.auth);
  const form = useRef();
  const checkBtn = useRef();
  const [category, setCategory] = useState("");
  const [unit, setUnit] = useState("");
  const [message, setMessage] = useState("");
  const [successful, setSuccessful] = useState(false);
  const [audit, setAudit] = useState(false);
  const [edit, setEdit] = useState(false);
  const [upload, setUpload] = useState(false);
  const [ritmNumber, setRitmNumber] = useState(null);
  const dispatch = useDispatch();
  const onChangeCategory = (e) => {
    const category = e.target.value;
    setCategory(category);
  };
  const onChangeUnit = (e) => {

    setUnit(e.target.value);
  };
  const onChangeAudit = (e) => {

    setAudit(!!!audit);
  };
  const onChangeEdit = (e) => {

    setEdit(!!!edit);
  };
  const onChangeUpload = (e) => {

    setUpload(!!!upload);
  };

  const handleLogin = (e) => {
    e.preventDefault();
    form.current.validateAll();
    dispatch(requestAccess('currentUser', category, unit, audit, upload, edit))
      .then((response) => {
        setRitmNumber(response.item);
      })
      .catch((response) => {
        setSuccessful(false);
      });
  };

  return (
    <>
      <div className="topPanelReq" >
        <h1 style={{ 'margin': '0 0 10px 0' }} >Request data access</h1>
        The following service allow to Data managers to request authoritation for handle data of an specific unit organitation
      </div>
      <Form onSubmit={handleLogin} ref={form}>
        <div className="formc"  >
          <div  className="form-group form">
            <label htmlFor="cat">Data category</label>
            <Input
              type="text"
              className="form-control"
              name="cat"
              value={category}
              onChange={onChangeCategory}
              validations={[required]}
            />
            <label htmlFor="unit">Organitation unit</label>
            <Input
              type="text"
              className="form-control"
              name="unit"
              value={unit}
              onChange={onChangeUnit}
              validations={[required]}
            />
            <div style={{ display: 'inherit' }}>

              <Input
                type="checkbox"
                className="form-control"
                name="audit"
                value={audit}
                onChange={onChangeAudit}
              />
              <label for="audit"> Audit</label>
            </div>
            <div style={{ display: 'inherit' }}>

              <Input
                type="checkbox"
                className="form-control"
                name="edit"
                value={edit}
                onChange={onChangeEdit}
              />
              <label for="upload"> Edit</label>
            </div>
            <div style={{ display: 'inherit' }}>

              <Input
                type="checkbox"
                className="form-control"
                name="upload"
                value={upload}
                onChange={onChangeUpload}
              />
              <label for="upload"> Upload evidence</label>
            </div>
            
            <Button text="Submit request" />
            {message && (
              <div className="form-group">
                <div className={successful ? "alert alert-success" : "alert alert-danger"} role="alert">
                  {message}
                </div>
              </div>
            )}
            {ritmNumber && (
              <div className="form-group">
                <div className={"alert alert-success"} role="alert">
                  Request submitted successfully!<br />
                  Request number is: <b>{ritmNumber}</b>
                </div>
              </div>
            )}
          </div>
        </div>
      </Form>
    </>
  );
};

export default Request;
