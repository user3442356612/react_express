import React from "react";
import { Redirect } from 'react-router-dom';
import { useSelector } from "react-redux";
import './Profile.scss'
const Profile = () => {
  const { user: currentUser } = useSelector((state) => state.auth);



  return (
    <div className="profcontainer">
      <h1>
        {currentUser.username}'s Profile
      </h1>
      <p>
        <strong>Token:</strong> {currentUser.token.substring(0, 20)} ...{" "}
        {currentUser.token.substring(currentUser.token.length - 20)}
        <br />
        <strong>Id:</strong> {currentUser.id}<br />
        <strong>Email:</strong> {currentUser.email}<br />
        <strong>Authorities:</strong>
        <ul>
          {!!currentUser.roles &&
            currentUser.roles.map((role, index) => <div key={index}>{role.name}<br/></div>)}
        </ul>
      </p>
    </div>
  );
};

export default Profile;
