import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Select from "react-validation/build/select";
import { isEmail } from "validator";
import { register } from "../actions/auth";
import Button from './Button'
import './Register.scss'

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Field required
      </div>
    );
  }
};

const validEmail = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        Email not valid, enter a valid email
      </div>
    );
  }
};

const vusername = (value) => {
  if(!value || !!parseInt(value[0]) || (value.length < 3 || value.length > 20))
  return (
    <div className="alert alert-danger" role="alert">
      {!value && <span>This field is required<br /></span>}
      {!!parseInt(value[0]) && <span>First character can't be number<br /></span>}
      {(value.length < 3 || value.length > 20) && <span>Username must be between 3 and 20 characters<br /></span>}
    </div>
  );

};

const vpassword = (value) => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
       <span>Password must be between 6 and 40 characters</span>
      </div>
    );
  }
};

const Register = () => {
  const form = useRef();
  const checkBtn = useRef();
  const [username, setUsername] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [english, setEnglish] = useState("");
  const [successful, setSuccessful] = useState(false);
  const dispatch = useDispatch();

  const onChangeUsername = (e) => {

    setUsername(e.target.value);
  };

  const onChangeName = (e) => {

    setName(e.target.value);
  };

  const onChangeEmail = (e) => {

    setEmail(e.target.value);
  };

  const onChangePassword = (e) => {

    setPassword(e.target.value);
  };

  const onChangeEnglish = (e) => {

    setEnglish(e.target.value);
  };

  const handleRegister = (e) => {
    e.preventDefault();

    setSuccessful(false);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      dispatch(register(username, name, english, email, password))
        .then(() => {
          setSuccessful(true);
        })
        .catch(() => {
          setSuccessful(false);
        });
    }
  };

  return (
    <div >
      <div >
        <div className="topPanelReg" >
          <h1 style={{ 'margin': '0 0 10px 0' }} >Register page</h1>
          The following page would register a new user
        </div>

        <Form onSubmit={handleRegister} ref={form}>
          {!successful && (
            <div className='form' >
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <Input
                  type="text"
                  className="form-control"
                  name="username"
                  value={username}
                  onChange={onChangeUsername}
                  validations={[vusername]}
                />
                <label htmlFor="name">Name</label>
                <Input
                  type="text"
                  className="form-control"
                  name="name"
                  value={name}
                  onChange={onChangeName}
                  validations={[required]}
                />

                <label htmlFor="email">Email</label>
                <Input
                  type="text"
                  className="form-control"
                  name="email"
                  value={email}
                  onChange={onChangeEmail}
                  validations={[required, validEmail]}
                />
                <label htmlFor="english">English level</label>
                <Select
                  type="select"
                  className="form-control"
                  name="english"
                  value={english}
                  onChange={onChangeEnglish}
                  validations={[required]}
                >
                  <option value="">Select level</option>
                  <option value="Beginner">Beginner level</option>
                  <option value="Intermediate">Intermediate level</option>
                  <option value="UperIntermediate">Uper Intermediate level</option>
                  <option value="Advanced">Advanced level</option>
                  <option value="C2">C2 level</option>
                </Select>
                <label htmlFor="password">Password</label>
                <Input
                  type="password"
                  className="form-control"
                  name="password"
                  value={password}
                  onChange={onChangePassword}
                  validations={[required, vpassword]}
                />
              </div>

              <div className="form-group">
                <Button text="Sign Up" />
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
        {successful && <span>User registered!</span>}
      </div>
    </div>
  );
};

export default Register;


/* {message && (
            <div className="form-group">
              <div className={ successful ? "alert alert-success" : "alert alert-danger" } role="alert">
                {message}
              </div>
            </div>
          )}*/