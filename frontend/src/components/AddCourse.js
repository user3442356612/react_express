import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from 'react-router-dom';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import Button from './Button'
import { login } from "../actions/auth";
import './Login.scss'

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const Addcourse = (props) => {
  const form = useRef();
  const checkBtn = useRef();
  const [course, setCoursename] = useState("");
  const [category, setCategory] = useState("");
  const [instructor, setInstructor] = useState("");
  const [loading, setLoading] = useState(false);
  const { isLoggedIn } = useSelector(state => state.auth);

  const dispatch = useDispatch();

  const onChangeCourseName = (e) => {
    const name = e.target.value;
    setCoursename(name);
  };

  const onChangeInstructor = (e) => {
    const ins = e.target.value;
    setInstructor(ins);
  };


  const onChangeCategory = (e) => {
    const cat = e.target.value;
    setCategory(cat);
  };  

  const handleLogin = (e) => {
    e.preventDefault();
    setLoading(true);
    form.current.validateAll();
    console.log(checkBtn.current.context._errors.length)
    if (checkBtn.current.context._errors.length === 0) {
      dispatch()
        .then(() => {
          props.history.push("/profile");
          window.location.reload();
        })
        .catch(() => {
          setLoading(false);
        });
    } else {
      setLoading(false);
    }
  };

  if (isLoggedIn) {
    return <Redirect to="/profile" />;
  }
  
  return (
 
      <div className="login">
        <div className="topPanelLog" >
        <h1 style={{ 'margin': '0 0 10px 0' }} >Add course page</h1>
        The following service for add a course
      </div>
        <Form  className="form" onSubmit={handleLogin} ref={form}>
          <div  className="form-group">
            <label htmlFor="name">Course name</label>
            <Input
              type="text"
              className="form-control"
              name="name"
              value={course}
              onChange={onChangeCourseName}
              validations={[required]}
            />

            <label htmlFor="instructor">Instructor</label>
            <Input
              type="text"
              className="form-control"
              name="instructor"
              value={instructor}
              onChange={onChangeInstructor}
              validations={[required]}
            />
            <label htmlFor="instructor">Category</label>
            <Input
              type="text"
              className="form-control"
              name="category"
              value={category}
              onChange={onChangeCategory}
              validations={[required]}
            />
          </div>
          <div className="form-group">
          <Button text="Add course"/>
          </div>
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
      </div>
  
  );
};

export default Addcourse;
