import React, { useState, useRef, useEffect } from "react";
import { useTable, useSortBy, usePagination, useGlobalFilter, useAsyncDebounce } from "react-table";
import PropTypes from 'prop-types';
import './Table.scss'

function GlobalFilter({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
}) {
  const count = preGlobalFilteredRows.length
  const [value, setValue] = React.useState(globalFilter)
  const onChange = useAsyncDebounce(value => {
    setGlobalFilter(value || undefined)
  }, 200)

  return (
    <span className="filterspan">
      Filter:{' '}
      <input
        className="filterinput"
        value={value || ""}
        onChange={e => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
        placeholder={`${count} records...`}

      />
    </span>
  )
}

export default function Table({ columns, dataT, updateMyData, onSelectRecord, onDeleteRecord }) {

  useEffect(() => {
    setPageSize(10);
  }, []);
  

  const EditableCell = ({
    value: value,
    row: { index,original },
    column: { id },
  }) => {

    //const [val, setValue] = useState(value);
    const [focus, setFocus] = useState(true);
    const myRef = useRef(null);
    const onChange = e => {
      value = e.target.value;
      console.log(e.target.value);
      updateMyData(index, id, e.target.value);
    }

    /*const onClick = e => {
      myRef.current.focus();
    }*/

   /* const onBlur = (e) => {
      updateMyData(index, id, e.target.value);
      setFocus(true);
    }*/

    /*const onFocus = () => {
      setFocus(false);
    }*/

    const onExpand = (e) => {
      updateMyData(index, id, e.target.value);
      setFocus(true);
    }


    /* useEffect(() => {
       setValue(initialValue)
       updateMyData(index, id, initialValue);
     }, [initialValue])*/

     
    if (id == "delete")
      return <button style={
        {'border': '1px solid grey',
            'background': 'none',
            'width': '100%',
            'height': '100%',
            'border-radius': '6px'}} onClick={()=>onDeleteRecord(original)}>Click</button>;
    else if (id.includes("date") || id.includes("createdAt") )
      {
        
        return <input style={{'border':'none'}} onChange={onChange} type="datetime-local" id="meeting-time"
        value={new Date(value).toISOString().substring(0,16)}/>
       
       }
    else
      return <input  onClick={()=>onSelectRecord(original)} className='cell' ref={myRef} value={value} /*onFocus={onFocus}*/ onChange={onChange} /*onBlur={onBlur}*/ />
  }

  const defaultColumn = {
    Cell: EditableCell,
  }

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    page,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize, globalFilter },
    preGlobalFilteredRows,
    setGlobalFilter,
  } = useTable({
    columns,
    data: dataT,
    defaultColumn,
    updateMyData,

  },
    useGlobalFilter,
    useSortBy,
    usePagination);

  return (
    <div>
      <div >
        <GlobalFilter
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={globalFilter}
          setGlobalFilter={setGlobalFilter}
        />
      </div>
      <table className='table' {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps()} {...column.getHeaderProps(column.getSortByToggleProps())} onClick={() => column.toggleSortBy(!column.isSortedDesc)} >{column.render("Header")}</th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row);
            return (
              <tr  {...row.getRowProps()}>
                {row.cells.map(cell => {
                 // console.log(row)
                  return <td  {...cell.getCellProps()} >{cell.render("Cell")}</td>;
                })}

              </tr>
            );
          })}
        </tbody>

      </table>
      <div className="pagination">
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {'<<'}
        </button>{' '}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {'<'}
        </button>{' '}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {'>'}
        </button>{' '}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {'>>'}
        </button>{' '}
        <span>
          Page{' '}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{' '}
        </span>
        <span>
          | Go to page:{' '}
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={e => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0
              gotoPage(page)
            }}
            style={{ width: '100px' }}
          />
        </span>{' '}
        <select
          value={pageSize}
          onChange={e => {
            setPageSize(Number(e.target.value))
          }}
        >
          {[3, 10, 20, 30, 40, 50].map(pageSize => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>
    </ div>
  );
}


Table.propTypes = {
  columns: PropTypes.array,
  dataT: PropTypes.array,
  setDataTable: PropTypes.func
}

Table.defaultProps = {
  columns: [],
  dataT: [],
  setDataTable: () => { }
};