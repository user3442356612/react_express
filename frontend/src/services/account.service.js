import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:3001/v2/";

const postAccount = (data) => {
  return axios.post(API_URL + "accounts", data,{headers: authHeader()});
};

const postAccounts = (data) => {
  return axios.post(API_URL + "accounts", data,{headers: authHeader()});
};


const getAccounts = () => {
  return axios.get(API_URL + "accounts",{headers: authHeader()});
};
const getAccountById = (id) => {
  return axios.get(API_URL + "accounts/"+id,{headers: authHeader()});
};

const updateAccountById = (id, account, client, opresponsible, team) => {
  return axios.put(API_URL + "accounts/"+id, {
    account,
    client,
    opresponsible,
    team
  },{headers: authHeader()});
};

const deleteAccount = (id) => {
  return axios.delete(API_URL + "accounts/"+id,{headers: authHeader()});
};

export default {
  postAccount,
  postAccounts,
  getAccounts,
  getAccountById,
  updateAccountById,
  deleteAccount
};
