import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:3001/v2/";

//return axios.get(API_URL + "admin", { headers: authHeader() });

const postLog = (id,user,team,start_date,end_date) => {
  console.log({
    id,
    user,
    team,
    start_date,
    end_date
  });
  return axios.post(API_URL + "logs/",{
    id,
    user,
    team,
    start_date,
    end_date
  },{headers: authHeader()});
};

const postLogs = (data) => {
  return axios.post(API_URL + "logs/",data,{headers: authHeader()});
};

const getLogs = () => {
  return axios.get(API_URL + "logs",{headers: authHeader()});
};
const getLogById = (id) => {
  return axios.get(API_URL + "logs/"+id,{headers: authHeader()});
};

const updateLogById = (id,user,team,start_date,end_date) => {
  console.log({
    id,
    user,
    team,
    start_date,
    end_date
  });
  return axios.put(API_URL + "logs/"+id.toString(), {
    id,
    user,
    team,
    start_date,
    end_date
  },{headers: authHeader()});
};

const deleteLog = (id) => {
  return axios.delete(API_URL + "logs/"+id,{headers: authHeader()});
};


export default {
  postLog,
  postLogs,
  getLogs,
  getLogById,
  updateLogById,
  deleteLog
};