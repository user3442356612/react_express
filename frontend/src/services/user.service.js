import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "http://localhost:3001/v2/";

const postUser = (id,username,password,roles,team) => {
  console.log({
    id,
    username,
    password,
    roles,
    team
  });
  return axios.post(API_URL + "users/",{
    id,
    username,
    password,
    roles,
    team
  },{headers: authHeader()});
};

const postUsers = (data) => {
  console.log(data);
  return axios.post(API_URL + "users/",data,{headers: authHeader()});
};

const getUsers = () => {
  return axios.get(API_URL + "users",{headers: authHeader()});
};
const getUserById = (id) => {
  return axios.get(API_URL + "users/",{headers: authHeader()});
};

const updateUserById = (id,username,password,roles,team) => {
  console.log({
    id,
    username,
    password,
    roles,
    team
  });
  return axios.put(API_URL + "users/"+id.toString(), {
    username,
    password,
    roles,
    team
  },{headers: authHeader()});
};

const deleteUser = (id) => {
  return axios.delete(API_URL + "users/"+id,{headers: authHeader()});
};


export default {
  postUser,
  postUsers,
  getUsers,
  getUserById,
  updateUserById,
  deleteUser
};