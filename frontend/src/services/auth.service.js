import axios from "axios";

const API_URL = "http://localhost:3001/v2/users/";

const register = (username, name, english, email, password) => {
  return axios.post(API_URL + "register", {
    username,
    name,
    english,
    email,
    password,
  });
};

const login = (username, password) => {
  return axios
    .post(API_URL + "login", {
      username,
      password,
    })
    .then((response) => {
      console.log("response")
      console.log(response)
      console.log("response.data")
      console.log(response.data)
      if (response.data) {
        let res = {...response.data.user,token:response.data.token};
        localStorage.setItem("user", JSON.stringify(res));
      }
      return response.data;
    });
};

const logout = () => {
  localStorage.clear();
};

export default {
  register,
  login,
  logout,
};
