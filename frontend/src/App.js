import React, { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Router, Switch, Route, Link, Redirect } from "react-router-dom";
import MainPage from "./pages/MainPage";
import EventBus from "./common/EventBus";
import { logout } from "./actions/auth";
import { clearMessage } from "./actions/message";
import { history } from "./helpers/history";
import Login from "./components/Login";
import Addcourse from "./components/AddCourse";
import Register from "./components/Register";
import Account from "./components/Account";
import User from "./components/User";
import Assignation from "./components/Assignation";
import Profile from "./components/Profile";
import Log from "./components/Log";
import "./App.scss";

const App = () => {
  const dispatch = useDispatch();
  const { user: currentUser } = useSelector((state) => state.auth);

  useEffect(() => {
    history.listen((location) => {
      dispatch(clearMessage()); // clear message when changing location
    });
  }, [dispatch]);
  const logOut = useCallback(() => {
    dispatch(logout());
  }, [dispatch]);
  useEffect(() => {
    EventBus.on("logout", () => {
      logOut();
    });

    return () => {
      EventBus.remove("logout");
    };
  }, [currentUser, logOut]);

  return (
    <div id="App" className="App">
       <Router history={history}>
        <div id="headbar" className="headbar">
          <div className="hleft">
         

          </div>
          <div className="hright">
            {currentUser ? (<Link to={'/user'} className="helements" style={{ 'color': 'white', 'text-decoration': 'none' }}> Users </Link>) : null}
            {currentUser ? <Link to={'/account'} className="helements" style={{ 'color': 'white', 'text-decoration': 'none' }}> Accounts </Link> : null}
            {currentUser ? <Link to={'/assigments'} className="helements" style={{ 'color': 'white', 'text-decoration': 'none' }}> Assigments </Link> : null}
            {currentUser ? (
              <>
                <a href="/" className="helements" style={{ 'color': 'white', 'text-decoration': 'none' }} onClick={logOut}>
                  LogOut
                </a>
                <Link to={"/profile"} >
                  <img alt="dsa" width="30" height="30" src="https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png" />
                </Link>
              </>
            ) : (
              <>
                <Link to={"/login"} className="helements" style={{ 'color': 'white', 'text-decoration': 'none' }}>
                  Login
                </Link>
                <Link to={"/register"} className="helements" style={{ 'color': 'white', 'text-decoration': 'none' }}>
                  Register
                </Link>
              </>
            )}
          </div>
        </div>
       

     
          <Switch>
            <Route exact path={"/"} component={()=><Redirect to='/login'/>} />
            <Route exact path={"/login"} component={Login} />
            <Route exact path={"/add"} component={Addcourse} />
            <Route exact path={"/register"} component={Register} />
            <Route exact path={"/user"} component={User} />
            <Route exact path={"/account"} component={Account} />
            <Route exact path={"/assignation"} component={Assignation} />
            <Route exact path={"/profile"} component={Profile} />
            <Route exact path={"/assigments"} component={Log} />
          </Switch>
      </Router>
      <div className="footer">
          <div className="fleft">Example project</div>
        </div>

    </div>
  );
};




export default App;



